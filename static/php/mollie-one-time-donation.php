<?php

# use unix file path notation for now
require_once __DIR__ . "/vendor/autoload.php";
require_once __DIR__ . "/functions.php";

// the testing key will be replaced with the live key in the build system
$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey("MOLLIEAPIKEY");

$payment_amount = (string)$_POST["donation_amount"] . ".00";

$payment = $mollie->payments->create([
    "amount" => [
        "currency" => "EUR",
        "value" => $payment_amount
    ],
    "description" => "Krita Foundation One-time donation",
    "redirectUrl" => calculate_redirect_url("one-time-donation-thank-you/") . "?amount=" . urlencode($payment_amount),
    "cancelUrl" => calculate_redirect_url("donations/"), // return to the donation page if cancelled
]);

$payment->redirectUrl .= "&payment_id=" . urlencode($payment->id) . "&payment_status=" . urlencode($payment->status);


$payment->update();

header("Location: " . $payment->getCheckoutUrl(), true, 303);

function calculate_redirect_url($rel_path)
{
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $full_url = $protocol . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $base_url = explode("php", $full_url)[0]; // get base domain name part of URL without the php folder

    // add language code
    $language_code = isset($_POST["lang"]) ? $_POST["lang"] : "en";
    return $base_url . $language_code . "/" . $rel_path;
}

?>

