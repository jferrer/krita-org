// script goes here which tries to auto-detect which version to download
// instead of how we did it, we could list them as a UL, then move the "smart" option to the top and make a button
// while the other options are lower and just look like links
function findOS() {
    var currentOS = '';
    if ( navigator.appVersion.indexOf("Win") != -1 ) { currentOS = "Windows";  }
    else if ( navigator.appVersion.indexOf("Mac") != -1) { currentOS = "MacOS"; }
    else if ( navigator.appVersion.indexOf("Android") != -1) { currentOS = "Android"; }
    else if( navigator.appVersion.indexOf("iPhone") != -1) { currentOS = "iPhone"; }
    else if( navigator.appVersion.indexOf("X11") != -1) { currentOS = "UNIX"; }
    else if( navigator.appVersion.indexOf("iPad") != -1 ) { currentOS = "iPad"; }
    else { currentOS="Linux"; }

    return currentOS;
}
let currentOS = findOS();


// show download button for appropriate OS
if(currentOS === 'Windows' ) {
    document.querySelector('#windows-download').setAttribute('style', 'display: block');
}
else if(currentOS === 'MacOS' ) {
    document.querySelector('#macOS-download').setAttribute('style', 'display: block');
}
else if( currentOS === "iPhone" || currentOS =='iPad' ) {
    document.querySelector("#ios-download-information").setAttribute('style', 'display: block');
}
else if(currentOS === "Android") {
    document.querySelector("#android-download-information").setAttribute('style', 'display: block');
}
else {
    // should be either Unix or Linux options
    document.querySelector('#linux-download').setAttribute('style', 'display: block');
}



// add event listener to all the download links to go to the post-download page after X seconds
function gotoPostDownloadPage() {
    location.href = `/${document.documentElement.lang}/post-download`;
}
for (elm of document.querySelectorAll('.downloadable')) {
    elm.addEventListener('click', event => {
        setTimeout(gotoPostDownloadPage, 5000) // second parameter is time to wait to go to download page 1s = 1000
    })
}
