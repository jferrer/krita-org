---
title: "Krita 5.1.0 正式版已经推出"
date: "2022-08-18"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.1.0 正式版，包含各种改进和新功能。

## 新版功能亮点

- 更多操作可被用于多个选中的图层
- 改进了 WebP、Photoshop 多层 TIFF、PSD 文件等的支持，新增对 JPEG-XL 文件格式的支持。(尚有未解决的问题，详情请见下文)
- 更换矢量优化程序库，使用 Xsimd 代替了 Vc，改善了绘画性能，并首次实现了安卓平台下的矢量优化。
- 填充工具的相连颜色填充功能得到了扩展。新增围选填充工具。
- Windows 版使用了新版的 ANGLE，改善了性能和显卡驱动程序的兼容性。
- 触摸控制现已支持在画布输入设置页面进行自定义操作，如“点击以撤销”。

除此之外我们还修复了数百个程序缺陷，针对程序多个方面改善了性能，对用户界面进行了进一步打磨，改进了动画系统 (但因为时间不足，音频系统的相关功能并未来得及合并到 5.1 系列，它们预计会在 5.2 系列加入)。

完整更新列表请见 [Krita 5.1 系列版本说明](https://krita.org/zh/krita-5-1-release-notes-zh/)。

您也可以在下方观看 Krita 5.1.0 的介绍视频 (中国大陆用户需要科学上网)：

{{< youtube  TnvCjziCUGI >}}

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是一个自由开源的软件项目，如果条件允许，请考虑通过[捐款](https://fund.krita.org)或[购买视频教程](https://krita.org/en/shop/)等方式为我们提供资金支持，这可以确保 Krita 的核心开发团队成员为项目全职工作。

![Screenshot of 5.1.0-beta1](images/posts/2022/5.1.0-beta1-1024x562.webp)

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- G'MIC 滤镜中文翻译：打开 G'MIC 滤镜 -> 设置 (左下角) -> 勾选“Translate Filters”。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-x86_64-5.1.0-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-x86-5.1.0-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-arm64-v8a-5.1.0-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.0/krita-armeabi-v7a-5.1.0-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 在 ChromeOS 下已经可以稳定用于生产用途，在一般安卓系统下仍处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭小细长的设备进行优化，也尚未开发平板专属的模式，因此建议搭配键盘使用。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。
- 比较新款的安卓平板一般使用 ARM CPU，Chromebook 等一般使用 Intel CPU。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- 请访问[下载目录页面](https://download.kde.org/stable/krita/5.1.0)，点击最右列的“Details”获取哈希值。

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.1.0/)。
