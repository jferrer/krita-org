---
title: "發佈 Krita 5.1.0 第二個測試版本 (Beta 2)"
date: "2022-07-15"
categories: 
  - "news_zh-tw"
  - "development-builds_zh-tw"
---

今天我們發佈了 Krita 5.1.0 的第二個測試版本。

想知道完整的新功能清單的話，可以看看還未寫完的[英文版發佈通告](https://krita.org/en/krita-5-1-release-notes/)喔！

您也可以觀看以下由 Wojtek Trybus 製作的更新內容影片：

{{< youtube  TnvCjziCUGI >}}

 

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是自由、免費及開源的專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或[購買教學影片](https://krita.org/en/shop/)支持我們吧！得到您們的熱心支持，我們才能夠讓核心開發者全職為 Krita 工作。

注意：Krita 5.1 Beta 1 測試版本中含有一個程式錯誤，或會導致筆刷預設儲存出錯，使筆刷在其他 Krita 版本中載入時出現軟體崩潰。如您曾使用 Beta 1 製作或修改筆刷預設，[這份 Python 指令稿](https://invent.kde.org/tymond/brushes-metadata-fixer)可為您修正這些筆刷預設。

在發佈 [Beta 1](https://krita.org/zh-tw/item/first-beta-for-krita-5-1-0-released_zh-tw/) 之後，我們修正了以下問題：

- 修正在切換作業空間時調色板變更。 (Fix changing workspaces selecting different palettes.) [BUG:446634](https://bugs.kde.org/show_bug.cgi?id=446634)
- 修正當資源資料夾非預設位置時載入範本的問題。 (Fix loading templates if the resource folder is in a non standard location.) [BUG:452706](https://bugs.kde.org/show_bug.cgi?id=452706)
- 修正在移除標籤時切換筆刷預設或會出現的軟體崩潰問題。 (Fix occasional crash selecting a preset when a tag is removed.) [BUG:454052](https://bugs.kde.org/show_bug.cgi?id=454052)
- 在儲存作業空間後清空作業空間名稱輸入方塊。 (Clear the name of a workspace after saving it.) [BUG:446652](https://bugs.kde.org/show_bug.cgi?id=446652)
- 修正色彩塗抹設定頁的循環更新問題。 (Fix cycling updates in the color smudge brush configuration page.) [BUG:455244](https://bugs.kde.org/show_bug.cgi?id=455244)
- 容許載入及儲存名稱中帶英文句號的資源到資源包內。 (Make it possible to save and load resources with dots in the name to bundles.) [BUG:453702](https://bugs.kde.org/show_bug.cgi?id=453702)
- 容許重覆使用 G'Mic 的「Curves」濾鏡。 (Make it possible to use the G'Mic curves filter more than once.) [BUG:455891](https://bugs.kde.org/show_bug.cgi?id=455891)
- 修正匯出調色板時的 Assert 陳述式。 (Fix an assert when trying to export a palette.) [BUG:454949](https://bugs.kde.org/show_bug.cgi?id=454949)
- 修正儲存文字到 PSD 時出現的軟體崩潰問題。 (Fix crash when saving an image with text to PSD.) [BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- 修正圖層色彩標籤中的無限遞迴問題。 (Fix infinite recursions in the layer color label selector.) [BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- 根據畫面 DPI 縮放設定選取區域外框斑點的大小。 (Set the size of the selection's marching ants based on the screen's dpi.) [BUG:456364](https://bugs.kde.org/show_bug.cgi?id=456364)
- 當不可見的文件中含有使用中選取區域時，降低閒置時的 CPU 使用率。 (CPU Reduce idle cpu usage when a selection is active in an image that is not currently shown.) [BUG:432284](https://bugs.kde.org/show_bug.cgi?id=432282)
- 修正在關閉設定視窗時或會出現的軟體崩潰問題。 (Fix crash when closing the settings dialog window under some circumstances.) [BUG:445329](https://bugs.kde.org/show_bug.cgi?id=445329)
- 修正在裁切影像後網格設定變為無效的問題。 (Fix issues with Grid settings becoming invalid when cropping.) [BUG:447588](https://bugs.kde.org/show_bug.cgi?id=447588)
- 修正在復原筆跡時影像投射崩壞的問題。 (Fix random artefacts in the image projection when undoing brush strokes.) [BUG:453243](https://bugs.kde.org/show_bug.cgi?id=453243)
- 提高圖案縮放上限至 10000%。 (Increase the maximum pattern scaling to 10000%.)
- 修正無法載入灰階檔案圖層時的 Assert 陳述式。 (Fix an assert when a grayscale file layer fails to load the image.) [BUG:456201](https://bugs.kde.org/show_bug.cgi?id=456201)
- 修正使用 G'Mic 濾鏡多個圖層輸入的圖層順序。 (Fix layer order when using multiple layer inputs for G'Mic filters.) [BUG:456463](https://bugs.kde.org/show_bug.cgi?id=456463)
- 修正拖放向量形狀圖層至不同解析度影像時出現的問題 (Fix drag and drop of shape layers when the source and destination images have a different resolution.) [BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- 修正在同時移除多個圖層時或會出現的軟體崩潰問題。 (Fix a crash when trying to remove multiple layers at once.)
- 修正拖放向量形狀圖層至不同影像時出現的軟體崩潰問題。 (Fix a crash when dragging and dropping a vector layer from one image to another.) [BUG:456450](https://bugs.kde.org/show_bug.cgi?id=456450)
- 修正導致在自動儲存時當機的 Assert 陳述式。 (Fix an assert in on document saving that caused freezes on autosaving.) [BUG:456404](https://bugs.kde.org/show_bug.cgi?id=456404)
- 使「閉合區域填充工具」依照影像界限操作。 (Make the enclose and fill tool take into account image limits.) [BUG:456299](https://bugs.kde.org/show_bug.cgi?id=456299)
- 修正旋轉方塊。 (Fix rotating rectangles.)
- 修正拖放插入為參考圖。 (Fix dropping reference images not working at all.) [BUG:456382](https://bugs.kde.org/show_bug.cgi?id=4563822)
- 改善繪製筆刷外框的效能。 (Improve the performance of the brush outline cursor.) [BUG:456080](https://bugs.kde.org/show_bug.cgi?id=456080)
- 修正拖放插入的圖層名稱。 (Fix the layer name for layers added through drag and drop.) [BUG:455671](https://bugs.kde.org/show_bug.cgi?id=455671)
- 修正貼上影像的位置。 (Fix positioning of pasted images.) [BUG:453247](https://bugs.kde.org/show_bug.cgi?id=453247)
- 修正當使用了混色模式、不可見度或擦除模式時填充工具的快速模式。 (Fix fast mode when blending mode, opacity or eraser is used in the fill tool.)
- 修正在對使用中選取區域使用濾鏡時出現的軟體崩潰問題。 (Fix a crash when trying to filter an active selection.) [BUG:455844](https://bugs.kde.org/show_bug.cgi?id=455844)
- 改善動畫不透明度關鍵影格更動的復原操作。 (Improve undo of opacity animation keyframe changes.) [BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- 修正在設定複製圖層的不透明度關鍵影格時出現的軟體崩潰問題。 (Fix a crash when setting an opacity keyframe on a copied layer.) [BUG:454547](https://bugs.kde.org/show_bug.cgi?id=454547)
- 修正在移動動畫變形遮罩時的 Assert 陳述式。 (Fix an assert when moving an animated transform mask)
- 修正預製筆刷的外框形狀。 (Fix brush outlines of predefined brushes.) [BUG:455912](https://bugs.kde.org/show_bug.cgi?id=455912)
- 在更改停駐點色彩時正確地更新網狀漸變。 (Update the meshgradient properly if the stop color is changed.)
- 修正更改向量形狀漸變的停駐點。 (Fix editing the stops of a vector shape gradient.) [BUG:455794](https://bugs.kde.org/show_bug.cgi?id=455794), [BUG:447464](https://bugs.kde.org/show_bug.cgi?id=447464), [BUG:449606](https://bugs.kde.org/show_bug.cgi?id=449606)
- 修正匯出 JPEG-XL 動畫的問題。 (Fix issues when exporting an animation to JPEG-XL.) [BUG:455597](https://bugs.kde.org/show_bug.cgi?id=455597), [BUG:455598](https://bugs.kde.org/show_bug.cgi?id=455598)
- 支援在 TIFF 影像中使用 YCbCr 及 JPEG 壓縮。 (Support YCbCr and JPEG compression in TIFF files.)
- 容許載入 1ppi 解析度的影像。 (Make it possible to load krita files with 1ppi resolution.) [BUG:444291](https://bugs.kde.org/show_bug.cgi?id=444291)
- 改善總覽圖的更新頻率。 (Improve the refresh rate of the overview docker.) [BUG:443674](https://bugs.kde.org/show_bug.cgi?id=443674)
- Android: 使用正確的全螢幕圖示。 (use the correct full-screen icon in the statusbar.) [BUG:456065](https://bugs.kde.org/show_bug.cgi?id=456065)
- Python: 在修改設定 annotation 時將影像設定為已修改。 (set the image modified when changing an annotation.) [BUG:441704](https://bugs.kde.org/show_bug.cgi?id=441704)
- 修正複製貼上參考圖。 (Fix copy/pasting of reference images.) [BUG:454515](https://bugs.kde.org/show_bug.cgi?id=454515)
- 修正在匯入輔助尺集合後復原操作引致軟體崩潰的問題。 (Fix crash on undo after importing an assistant set.) [BUG:455584](https://bugs.kde.org/show_bug.cgi?id=455584)
- 修正在使用 OpenColorIO 時的效能問題。 (Fix slowdowns when using OpenColorIO.)
- 修正消失點和橢圓輔助尺的可見度設定。 (Fix visibility for Vanishing Point Assistant and Ellipse Assistant.)
- Python: 使「Node::setColorSpace」在找不到指定色彩設定檔時傳回「false」。 (make Node::setColorSpace return false if the specified profile couln't be retrieved.) [BUG:454812](https://bugs.kde.org/show_bug.cgi?id=454812)
- 修正在設定「單指拖曳」手勢至「縮放及旋轉畫布」時的軟體崩潰的問題。 (Fix a crash after assigning "one finger drag" to zoom and rotate canvas.) [BUG:455241](https://bugs.kde.org/show_bug.cgi?id=455241)
- 修正在影像操作進行中時開啟 G'Mic 引致軟體崩潰的問題。 (Fix a crash when opening G'Mic when the image is busy.)
- 最佳化所有 SVG 圖示。 (Optimzie all svg icons.)

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.0-beta2-setup.exe](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.0-beta2.zip](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x64-5.1.0-beta2-dbg.zip)

### Linux

- 64 位元 Linux: [krita-5.1.0-beta2-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.0-beta2.dmg](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86_64-5.1.0-beta2-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-x86-5.1.0-beta2-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-arm64-v8a-5.1.0-beta2-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-armeabi-v7a-5.1.0-beta2-release-signed.apk)

### 原始碼

- [krita-5.1.0-beta2.tar.gz](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.gz)
- [krita-5.1.0-beta2.tar.xz](https://download.kde.org/unstable/krita/5.1.0-beta2/krita-5.1.0-beta2.tar.xz)
