---
title: "Krita 5.1.0 現已正式推出！"
date: "2022-08-18"
categories: 
  - "news_zh-tw"
  - "officialrelease_zh-tw"
---

今天我們發佈了 Krita 5.1.0 正式版本。Krita 5.1 是一個重大功能更新喔！

## 重點新功能

- 更多操作可支援同時處理多於一個已選取圖層。
- 我們改善了對 WebP 檔案、帶有 Photoshop 圖層的 TIFF 檔案，以及 Photoshop 檔案的支援。同時，我們亦新增了支援 JPEG XL 檔案格式。
- Krita 現在使用了 XSIMD 程式庫來取代 Vc 作 SIMD（單指令多資料流）加速，以進一步提升筆刷效能。這對於在 Android 裝置上運行的效能影響尤其大，因為這是首個能支援 ARM 架構的 SIMD 加速指令集的 Krita 版本。
- 填充工具已擴展至支援「多重填充」（連續模式），另外也新增了一個「閉合區域填充工具」。
- 我們為 Windows 版本更新了 ANGLE 程式庫，以改善圖形加速的顯示卡兼容性和效能。
- 您現在可在「畫布輸入設定」中設定觸控手勢操作，如使用兩指點選作復原。

除此之外，當然還有大量各色各樣的錯誤修正、效能提升，以及在介面方面的修飾。 若希望閱讀更詳細的更動資訊，請移玉步到 [Krita 5.1 發佈通告](https://krita.org/zh-tw/krita-5-1-release-notes_zh-tw/)一頁。

{{< youtube  TnvCjziCUGI >}}

 

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是自由、免費及開源的專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或[購買教學影片](https://krita.org/en/shop/)支持我們吧！得到您們的熱心支持，我們才能夠讓核心開發者全職為 Krita 工作。

[![5.1.0-beta1 介面截圖](images/5.1.0-beta1-1024x562.png)](https://krita.org/wp-content/uploads/2022/06/5.1.0-beta1.png)

## 下載

### Windows

如果您使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.0-setup.exe](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.0.zip](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-dbg.zip)

### Linux

- 64 位元 Linux：[krita-5.1.0-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果您正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.0.dmg](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-x86_64-5.1.0-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-x86-5.1.0-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-arm64-v8a-5.1.0-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-armeabi-v7a-5.1.0-release-signed.apk)

### 原始碼

- [krita-5.1.0.tar.gz](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.gz)
- [krita-5.1.0.tar.xz](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.1.0/](https://download.kde.org/stable/krita/5.1.0) 並點選各下載檔案的「Details」連結以查閱該檔案的 MD5 / SHA1 / SHA256 校對碼。

### 數位簽章

Linux AppImage 以及原始碼的 .tar.gz 和 .tar.xz 壓縮檔已使用數位簽章簽名。您可以從[這裡](https://files.kde.org/krita/4DA79EDA231C852B)取得 GPG 公鑰。簽名檔可於[此處](https://download.kde.org/stable/krita/5.1.0/)找到（副檔名為 .sig）。
