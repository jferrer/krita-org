---
title: "發佈 Krita 5.1.1 修正版本"
date: "2022-09-14"
categories: 
  - "news_zh-tw"
  - "officialrelease_zh-tw"
---

今天我們發佈了 Krita 5.1.1 正式版本。這是一個修正版本，主要修正了兩項重大問題——一項是啟動軟體異常耗時的問題，另一項是在複製向量圖層時出現軟體崩潰的問題。

另外，此更新亦包含了其他問題修正：

- 修正 macOS 系統原生觸控板手勢的支援。[BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)
- 修正 Android 版本因置換檔案未有被刪除而導致應用程式佔用空間增加的問題。
- 修正 Android 版本中一項或會導致啟動軟體時出現軟體崩潰的問題。[BUG:458907](https://bugs.kde.org/show_bug.cgi?id=458907)
- 修正數項 MyPaint 筆刷引擎的問題。MyPaint 橡皮刷現使用了正確的混色模式。而在使用 MyPaint 筆刷時，沒有作用的混色模式選單會被停用。[BUG:453054](https://bugs.kde.org/show_bug.cgi?id=453054), [BUG:445206](https://bugs.kde.org/show_bug.cgi?id=445206)
- 動畫匯出：修正匯出影像序列的「起始編號」選項。[BUG:458997](https://bugs.kde.org/show_bug.cgi?id=458997)
- 修正 kritadefault.profile 畫布輸入設定檔被移除後出現的軟體崩潰問題。
- 在讀取 ACO 調色板時，載入色票名稱。[BUG:458209](https://bugs.kde.org/show_bug.cgi?id=458209)
- 修正選取圖層時出現軟體崩潰的問題。[BUG:458546](https://bugs.kde.org/show_bug.cgi?id=458546)
- 改善下列滑動條的調整級數：淡化、寛高比、近似色選取閾值
- 在移動可上色圖層節點時，只更新畫布一次。
- 在所有平台上修正使用多個輔助尺時 OpenGL 畫布出現黑色方塊的問題。[BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- 改善讀取 ZIP 檔案（包括 .kra、.ora 檔）的效能。
- 修正開啟單圖層 PSD 檔案的問題。[BUG:458556](https://bugs.kde.org/show_bug.cgi?id=458556)
- 將開始畫面上的更新通知連結設定為可點選。[BUG:458034](https://bugs.kde.org/show_bug.cgi?id=458034)
- JPEG-XL：修正線性 HDR 影像匯出和 16 位元浮點色匯入功能。[BUG:458054](https://bugs.kde.org/show_bug.cgi?id=458054)

 

## 下載

### Windows

如果您使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.1-setup.exe](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.1.zip](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-dbg.zip)

### Linux

- 64 位元 Linux: [krita-5.1.1-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果您正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.1.dmg](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86_64-5.1.1-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86-5.1.1-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-arm64-v8a-5.1.1-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-armeabi-v7a-5.1.1-release-signed.apk)

### 原始碼

- [krita-5.1.1.tar.gz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.gz)
- [krita-5.1.1.tar.xz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.1.1/](https://download.kde.org/stable/krita/5.1.1) 並點選各下載檔案的「Details」連結以查閱該檔案的 MD5 / SHA1 / SHA256 校對碼。

### 數位簽章

Linux AppImage 以及原始碼的 .tar.gz 和 .tar.xz 壓縮檔已使用數位簽章簽名。您可以從[這裡](https://files.kde.org/krita/4DA79EDA231C852B)取得 GPG 公鑰。簽名檔可於[此處](https://download.kde.org/stable/krita/5.1.1/)找到（副檔名為 .sig）。
