---
title: "Krita 5.2 Release Notes"
date: "2023-01-09"
---

After 5.0's big resource rewrite and 5.1's general improvements, we decided that Krita 5.2 should focus on tackling some of the biggest pain points in Krita. As such, this release contains a lot of background work that we hope to build future improvements on top of, as well as a healthy assortment of new features and fixes!

## Animation

We're happy to share that two major pain points for animation got tackled: synchronized playback of audio ([MR 1323](https://invent.kde.org/graphics/krita/-/merge_requests/1323)) and simplifying video export ([MR 1599](https://invent.kde.org/graphics/krita/-/merge_requests/1599)).

 {{< video-player src="images/posts/2023/animatic_waiting.mp4" type="video/mp4" >}}

In order to fix various audio-visual sync issues when playing animations with attached audio, **Emmet** and **Eoin** reworked much of the animation playback to use the MLT framework behind the scenes. [MLT](https://www.mltframework.org/) is a proven and flexible framework used by video editing programs like [Kdenlive](https://kdenlive.org/en/) and designed with frame-by-frame synchronization in mind, and should help Krita animators feel confident that their key frames will stay lined up with their voice work or background music, both inside of the program and in their exported animation videos.

Speaking of the video export option, [FFmpeg](https://ffmpeg.org/) is a massively important program that manages (among other things) rendering and conversion of audio and video formats. Previously, Krita required users to point to an FFmpeg executable somewhere on their system in order to use many of the features pertaining to video, such as animation video export, video import as animation, and the Recorder Docker for recording your painting sessions. This was mostly good enough for studio use, but it was still difficult to set up for users who aren't that technical, and almost impossible to do on locked down systems like Android or even a school computer. To make this better for almost everyone, we spent some time to include a basic build of FFmpeg into Krita itself, which should include "out-of-the-box" support for every free and open container and codec format under the sun. (Along with **Emmet** and **Eoin**, big thanks to **Amyspark**, **Ivan, Sharaf** and **Dmitry** who put in a lot of effort to help get every possible format building and working across various platforms.) Oh, and by the way, Krita continues to support the use of alternative FFmpeg binaries in the rare case that you need to additional features that we aren't able to provide.

_⚠️ Note: As of the writing this, animation export via FFmpeg is sadly still not working on Krita for Android due to restrictions of the platform itself, but we are looking for solutions to this going forward._

## Text

Because our original one didn't give artists enough control over the underlying text (making it hard to use and extend, as well as write a better text tool on top of), **Wolthera** completely rewrote Krita's text layout engine.


![](images/posts/2023/krita_5_2_svg_sample.png)

Demonstration of some new text layout features include (but are not limited to): text-in-shape, text-on-path and color font support.

With the new layout engine, we can handle everything the old engine could, as well as text-on-path, vertical text, and wrapped text and text in shape. We can now also access OpenType features as well as render emoji (bitmap and colrV0 types) ([MR 1607](https://invent.kde.org/graphics/krita/-/merge_requests/1607), [MR 1767](https://invent.kde.org/graphics/krita/-/merge_requests/1767)).

This was no small feat and is only just laying the foundation for more improvements to come! For Krita 5.2, you’ll still have to use the SVG code editor to access these new features, but for Krita 5.3 we’ll be working on the text tool proper, making it on-canvas and allowing you to configure the new features with menus and presets.

## Tools

**Dmitry Kazakov** overhauled the cumulative undo feature ([MR 1780](https://invent.kde.org/graphics/krita/-/merge_requests/1780)), this feature allows merging undo operations, which is useful when painting many strokes. We’ve simplified the code and made the options more intuitive to use.

The ability to anti-alias the results of the Sketch Brush Engine has been added by **Przemysław Gołąb** ([MR 1425](https://invent.kde.org/graphics/krita/-/merge_requests/1425)).

**Freya Lupen** added the ability to transform all selected layers at once with the transform tool ([MR 1792](https://invent.kde.org/graphics/krita/-/merge_requests/1792)).

### Fill Tool

**Deif Lou** has added a new mode to the fill tool: _Fill areas of similar color_ ([MR 1577](https://invent.kde.org/graphics/krita/-/merge_requests/1577)). In addition, both the fill tool and the enclose fill tool have gained _Stop growing at the darkest and/or most opaque pixels_ and _Fill all regions until a specific boundary color_ ([MR 1549](https://invent.kde.org/graphics/krita/-/merge_requests/1549), [1560](https://invent.kde.org/graphics/krita/-/merge_requests/1549)), as well as a toggle to use the same blending mode as the brush tool, or to have its own ([MR 1749](https://invent.kde.org/graphics/krita/-/merge_requests/1749)).


![Four images next to one another, the first only shows a dot, the rest show how starting a fill at that dot will have different fill results with different options.](images/posts/2023/fill_tool_stop_growing.png)

From the manual, filling the example in image A at the red dot will result in B for regular fill, in C with expanding the fill with a number of pixels and in D with the 'fill to boundary color' enabled.

### Selection Tool

The Contiguous Selection Tool also received the same selection extending option as the Fill tool ([MR 1549](https://invent.kde.org/graphics/krita/-/merge_requests/1549)), and the ability to set the opacity of the selection decoration ([MR 1697](https://invent.kde.org/graphics/krita/-/merge_requests/1697)). Furthermore, the selection decoration has been made DPI-aware ([MR 1774](https://invent.kde.org/graphics/krita/-/merge_requests/1774)).

### Shortcuts

Several new actions have been added:

![Showing select-layers-menu in action: An on-canvas menu with the layers of the image shown, stating 'rain_shining, rain_simple, clouds and select all layers](images/posts/2023/krita_5_2_menu_layer_selection.png)


Demonstrating the “Select Layers Menu” option, this menu shows the layers under the cursor.

- _Toggle Eraser Preset_ by **Freya Lupen**, ([MR 1689](https://invent.kde.org/graphics/krita/-/merge_requests/1689)) allows you to switch to the preset that would otherwise be stored for the ‘eraser’ end of the tablet stylus. Not all stylii have an eraser side, and some people really prefer to activate it by hotkey.
- _Sample Screen Color_ by **killy |0veufOrever**, ([MR 1720](https://invent.kde.org/graphics/krita/-/merge_requests/1720)) allows you to select a color anywhere on the screen, even outside Krita, similar to the Sample button in the _Select a Color_ dialogue.
- _Select Layers From Menu_ canvas input setting by **killy |0veufOrever**, ([MR 1766](ttps://invent.kde.org/graphics/krita/-/merge_requests/1766)) allows you to select a layer on-canvas from a dropdown menu.
- Krita now have a Clip Studio Paint compatible shortcut scheme, courtesy of **Freya Lupen**, ([MR 1565](https://invent.kde.org/graphics/krita/-/merge_requests/1565)).
- Krita can now detect conflicts in the canvas input setting shortcuts thanks to **Sharaf Zaman**, ([MR 1725](https://invent.kde.org/graphics/krita/-/merge_requests/1725)).

We have no shortage of plans for how to make things better, but only with stable community contributions can we keep a core team of professional developers working on Krita.

{{< support-krita-callout >}}
 

## Dockers

**Mathias Wein** brought us a ‘Wide Gamut Color Selector’. This selector almost the same as the advanced color selector, except it’s capable of selecting colors in wide-gamuts instead of just sRGB. We eventually want to remove the Advanced Color Selector in favor of this one, when we’re sure we haven’t lost any functionality ([MR 1600](https://invent.kde.org/graphics/krita/-/merge_requests/1600)).


![Wide gamut color selector is shown here as a gradient-square with a rainbow colored circle around it.](images/posts/2023/krita_5_2_wide_gamut.png)


The Layers docker got some extra display options:

- On Android, selecting multiple layers is tricky, therefore **Sharaf** implemented extra checkboxes on the side of the layers ([MR 1665](https://invent.kde.org/graphics/krita/-/merge_requests/1665)).
- **Freya Lupen** added the ability to see extra information about the layer opacity and blending modes ([MR 1501](https://invent.kde.org/graphics/krita/-/merge_requests/1501), [MR 1520](https://invent.kde.org/graphics/krita/-/merge_requests/1520), [MR 1615](https://invent.kde.org/graphics/krita/-/merge_requests/1615)), as well as making the automatic layer suffixes optional [MR 1498](https://invent.kde.org/graphics/krita/-/merge_requests/1498). On top of that, it’s now possible to change the scaling filter of file-layers ([MR 1784](https://invent.kde.org/graphics/krita/-/merge_requests/1784)).
- **Măcelaru Tiberiu** has ensured the brush preset docker looks good in horizontal mode: [MR 1670](https://invent.kde.org/graphics/krita/-/merge_requests/1670).
- Brush Preset History is now configurable ([MR 1623](https://invent.kde.org/graphics/krita/-/merge_requests/1623)).
- Undo, Redo and more for the palette docker ([MR 1617](https://invent.kde.org/graphics/krita/-/merge_requests/1617)).

## File Formats

We changed how CMYK blending modes work (with a toggle in the config), ([MR 1796](https://invent.kde.org/graphics/krita/-/merge_requests/1796)), this aligns the blending modes to the way Photoshop handles blending modes in CMYK, simplifying exchange of PSD files with clients that require CMYK PSDs.

**Rasyuqa A. H.** has been improving the JPEG-XL saving and loading code, implementing CMYK for JPEG-XL, improving compression by giving the JXL library more color space information, better metadata handling and support for saving and loading raster layers to JPEG-XL, ([MR 1656](https://invent.kde.org/graphics/krita/-/merge_requests/1656), [1693](https://invent.kde.org/graphics/krita/-/merge_requests/1693), [1673](https://invent.kde.org/graphics/krita/-/merge_requests/1673), [1722](https://invent.kde.org/graphics/krita/-/merge_requests/1722), [1795](https://invent.kde.org/graphics/krita/-/merge_requests/1795)). He also improved WebP compression ([MR 1785](https://invent.kde.org/graphics/krita/-/merge_requests/1785)), as well improving ICC transfer characteristic code ([MR 1667](https://invent.kde.org/graphics/krita/-/merge_requests/1667) and [1690](https://invent.kde.org/graphics/krita/-/merge_requests/1690)).


![A side-by-side comparison of the internal XYB profile and the original image profile. The example consists of a closeup of a cartoon eye that shows visible ringing with the original profile.](images/posts/2023/jxl-profile.png)


By using JPEG's XYB color space instead of the original profile, the JPEG-XL encoder is able to give much better results both in terms of artifacts and compression on images with extreme highlights, like those that are 40 times as bright as regular white.

- **Cedric Ressler** has improved EXR multi-layer handling ([MR 1677](https://invent.kde.org/graphics/krita/-/merge_requests/1677)).
- **Amyspark** has improved the RAW import, both the UI ([MR 1679](https://invent.kde.org/graphics/krita/-/merge_requests/1679)) and sped it up by using tiles ([MR 1694](https://invent.kde.org/graphics/krita/-/merge_requests/1694)).
- **Amyspark** has also improved the webP exporter by adding better metadata handling and animation support. ([MR 1468](https://invent.kde.org/graphics/krita/-/merge_requests/1468)).

## Other

One of the other big technical updates we did was to rewrite the brush settings code to work with the library Lager. Our old code had the brush presets and the widgets entangled in increasingly convoluted ways, which made it hard to extend the settings. We hope to use this work as a basis to redesign the brush settings widget ([MR 1334](https://invent.kde.org/graphics/krita/-/merge_requests/1334)).

 {{< video-player src="images/posts/2023/wrap-around-mode-direction.mp4" type="video/mp4" >}}

Wrap around mode can now be limited to vertical or Horizontal direction, making it simpler to creating looping backgrounds.

- **Freya Lupen** has added wrap-around directions for the wraparound mode ([MR 1524](https://invent.kde.org/graphics/krita/-/merge_requests/1524)).
- **Freya Lupen** has also added ability to remove single Recent Document entries ([MR 1666](https://invent.kde.org/graphics/krita/-/merge_requests/1666)).
- **Joshua Goins** has improved the tablet tester, so it now has access to tilt data ([MR 1678](https://invent.kde.org/graphics/krita/-/merge_requests/1678)).
- **Sharaf Zaman** implemented easier resource location selection for android ([MR 1771](https://invent.kde.org/graphics/krita/-/merge_requests/1771)).
- **Stephen Wilson** has made sure that Krita resets document metadata when using a template ([MR 1769](https://invent.kde.org/graphics/krita/-/merge_requests/1769)).
- **Agata Cacko** has spent a significant amount of time on getting better display names of color profiles ([MR 1768](https://invent.kde.org/graphics/krita/-/merge_requests/1768)).
- **Amyspark** has been cleaning up the UI left and right ([MR 1683](https://invent.kde.org/graphics/krita/-/merge_requests/1683), [MR 1696](https://invent.kde.org/graphics/krita/-/merge_requests/1696), [MR 1702](https://invent.kde.org/graphics/krita/-/merge_requests/1702), [MR 1701](https://invent.kde.org/graphics/krita/-/merge_requests/1701), [MR 1732](https://invent.kde.org/graphics/krita/-/merge_requests/1732), [MR 1744](https://invent.kde.org/graphics/krita/-/merge_requests/1744), [MR 1742](https://invent.kde.org/graphics/krita/-/merge_requests/1742), [MR 1772](https://invent.kde.org/graphics/krita/-/merge_requests/1772)).
- **Shuqi Xiu** has added a Lambert shading blending mode ([MR 1566).](https://invent.kde.org/graphics/krita/-/merge_requests/1566)

# Thank You

And that’s it! We hope that Krita 5.2 has a little something to help everyone make art.

Finally, a massive thank you to the everybody involved, including all of our [contributors](https://docs.krita.org/en/contributors_manual/community.html), [development fund members](https://fund.krita.org/) and, of course, our [community of artists](https://krita-artists.org/). As an open source and community-driven project, Krita simply wouldn’t be what it is today without an entire community of people testing, coding, writing documentation, creating brushes, translating, chipping in, spreading the word, and generally supporting the project however they can.

And if you're reading that wondering if you can help too, don't hesitate to get in touch--because there's always more to be done on our mission to make a better tool for every human artist.

{{< support-krita-callout >}}
