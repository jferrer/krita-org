---
title: "Report a Bug"
date: "2018-05-13"
layout: simple
---

Krita developers do their best to bring you a stable program but with so many features and multiple platforms to support (Windows, Linux, macOS, Android, ChromeOS) it’s inevitable that something will go wrong somewhere.

What to do if you suspect a bug.

1. Describe the problem on our forum [Krita-Artists.org](https://krita-artists.org). The community will help you determine if the cause is a software bug or a problem with your device. Either way, you'll get help.  
Before you hit submit, please be sure to have included:
    * A complete description of the problem and if possible complete screenshot of the entire Krita window showing the problem.
    * Your operating system (Windows, Linux, macOS, Android or ChromeOS)
    * Krita version number (go to Help > About Krita)

2. Once it's determined this is a bug, go ahead and make a formal bug report at  [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=krita).

Need additional help reporting a bug?
[How to complete a bug report](https://docs.krita.org/en/untranslatable_pages/reporting_bugs.html#be-complete-and-be-completely-clear)

Do **not** mail the Krita Foundation or developers directly for support. Krita has millions of users, and only half a dozen to a dozen developers!
