---
title: "Google Summer of Code, 2022 edition!"
date: "2022-05-25"
categories: 
  - "news"
---

This year's Google Summer of Code is different from previous editions: there are both larger and smaller projects possible, and it's no longer limited to students. We have two participants this year: Reinold Rojas and Xu Che.

Reinold Rojas previous cleaned up and improved the recorder docker, added color sample preview to the color sampler and fixed a number of bugs. This time, he will be working on exporting a Krita image to an SVG file. You can already save a vector layer as an SVG file, but not a complete Krita image. The plan is to preserve the layer structure of the Krita image, with vector layers staying vector layers and raster layers becoming embedded PNG images. Of course, not everything you can do in a Krita image can be saved to SVG, and Krita will warn you if you will lose information.

Xu Che has already started a proof of concept of his project: implementing pixel-perfect ellipses for raster layers, making this tool suitable for pixel art. This is something many people have asked for before, as in this [feature request](https://bugs.kde.org/show_bug.cgi?id=387242), so it's very exciting to see this happening now!
