---
title: "Interview with J2J-Art"
date: "2022-04-25"
categories:
- artist-interview
coverImage: "Girl-Redraw.webp"
---

### ![](images/posts/2022/Vision.webp)

### Could you tell us something about yourself?

My name is Jessica, but my art name is J2J-Art. I am a self-taught artist, I was born in 2004 in South Africa and I have only been creating art for 4 years now.

I draw both digitally and with graphite pencils on paper.

### Do you paint professionally, as a hobby artist, or both?

So far I am a hobby artist because I am still in school, so I only draw in my spare time, but one day I plan to sell my art on my own website.

### What genre(s) do you work in?

I don't think I have a fixed genre, but if I had to choose it would be a mix between fantasy art and semi-realism.

### Whose work inspires you most -- who are your role models as an artist?

I don't have many art role models, but I have been inspired by Rossdraws, Jessica Smith Art and Bowman Art.

### How and when did you get to try digital painting for the first time?

I started digital art only 7 months ago, so I am still very much a beginner, but before that I only drew on paper. Now I digitally paint with an old laptop and a built in mouse - I don't have a drawing tablet. At first I used Microsoft Paint, but that didn't last long because it isn't that great and it limited me.

### ![](images/posts/2022/Ephraim-study.webp)

### What makes you choose digital over traditional painting?

I don't paint traditionally, but I love to draw on paper with my graphite pencils. Both digital drawing and traditional drawing has their pros and cons, one of the advantages of digital art is the undo button and the liquify tool, which I use often.

### How did you find out about Krita?

Microsoft Paint was a real pain to use and I could barely draw anything because of the lack of functionality and tools, so I went on the internet and looked up free painting software and Krita was one of the first ones that popped up. It sounded really great to me and after doing more research I decided to give it a try.

### What was your first impression?

At first it was a little overwhelming with all the tools and settings, but once I started to understand everything it became so much easier.

### ![](images/posts/2022/Portrait.webp)

### What do you love about Krita?

I love almost everything about Krita, it is amazing user friendly software with so many tools! Personally, I love the liquify tool and of course all the brushes. Krita is the best software for digital painting I have seen so far and it really lets you explore your creativity and pushes you past your comfort zone. The blending modes are fantastic and that combined with the HSV/HSL Adjustment really brings any drawing to life.

### What do you think needs improvement in Krita?

The text tool is a little hard to navigate and the lasso tool could be a bit smoother and less jagged. On top of that, personally, I battle with picking the right skin colors so maybe the developers could add a category under the help section in Krita were they give RGB codes of real skin colours to help beginners such as myself.

### Is there anything that really annoys you?

Sometimes if I have a file that's too big Krita tends to crash a lot and lag.

### What sets Krita apart from the other tools that you use?

Well, it's a million times better than Microsoft Paint and it's an amazing program. I have looked into paid drawing software in the past, but I couldn't afford any of them and their reviews are not as great as the ones I have seen on Krita. Most of Krita's reviews are very positive.

### If you had to pick one favorite of all your work done in Krita so far, what would it be, and why?

![](images/posts/2022/Girl-Redraw.webp)This is my favorite so far because this is when I started to establish my painting technique and when I started to understand colour theory better. After this piece my digital art improved quite a bit as well.

### What techniques and brushes did you use in it?

I used the \_Ink-1\_Precision to make the lineart and the \_Airbrush\_Soft, \_Basic-5\_Size and the \_Basic-6\_Details for the shading. For the hair I used the \_Chalk\_Grainy brush and for the subtle details on the face I used the \_Texture\_Spray brush.

### Where can people see more of your work?

They can check out my Facebook page via this link: [https://www.facebook.com/J2J-Art-103096568981291](https://www.facebook.com/J2J-Art-103096568981291). And my krita-artists  account via this link: [https://krita-artists.org/u/j2j-art/activity/portfolio](https://krita-artists.org/u/j2j-art/activity/portfolio)

 ![](images/posts/2022/woman-with-yellow-hair.webp)

### Anything else you'd like to share?

I would like to thank [krita-artists.org](https://krita-artists.org) for their support and warm welcome. I would also like to thank the developers/moderators who have spent countless hours to create this amazing drawing software. Krita has really helped me as an artist and not only has it opened doors for me concerning digital art, but it has also encouraged me to improve my traditional art as well. Thanks to Krita my art is looking much better and I will never look back! Krita has to be the best free drawing software out there and I strongly believe it could be the next Photoshop!
