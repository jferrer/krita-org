---
title: "Only Download Krita From Trusted Places"
date: "2022-09-28"
---

We were informed that there are compromised downloads of Krita being offered, from Russian domains:

- https://krita-soft.ru
- https://krita-rus.ru

Please only download Krita from trusted sources: https://krita.org, https://download.kde.org, the Windows Store, Steam, the Epic Store and the Google Play Store (we're working on the Apple macOS store...) or your Linux distribution's package manager.

Third party download sites are outside our control, and can be compromised!
