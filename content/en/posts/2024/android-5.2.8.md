---
title: "Krita for Android Update"
date: "2024-11-22"
---

We have updated Krita for Android and ChromeOS in the Google Play Store to 5.2.8, an Android/ChromeOS-only emergency release.
This release fixes startup problems that happened on some devices with 5.2.6. Krita 5.2.8 for Android
is now available both for beta-track users as well as in the "stable" release track. Note, however,
that we still recommend treating Krita on Android as a beta release that might have bugs that impair
your work, as well as a user interface that is not optimized for touch devices.
