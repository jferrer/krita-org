---
title: "Krita Monthly Update – Edition 18"
date: "2024-09-12"
---
Welcome back! This monthly ‘zine is curated for you by the[ @Krita-promo](https://krita-artists.org/groups/krita-promo) team.
This edition covers two months' worth of updates.

## Development Report

Grum999 is working on [Grids with Unit Management MR 2201](https://invent.kde.org/graphics/krita/-/merge_requests/2201) which will allow users to select a grid measurement other than pixels (i.e., inches, mm. pt.). Grum is asking for [help with testing in Windows and Linux](https://krita-artists.org/t/grids-with-units/99244).

![Grids with units screenshot](images/posts/2024/mu18_grid_units-grum999.png)

Wolthera introduced [Font Selection Properties](https://krita-artists.org/t/text-tool-thread/57973/132) in the text tool thread on Krita-Artists.

Demo:

{{< video-player src="videos/posts/2024/wolthera-text-properties-font-basic-small.mp4" type="video/mp4" >}}

You can assist by reading the newest post from Wolthera and commenting.

latest update ([latest blog post](https://wolthera.info/2024/07/making-sense-of-font-selection/)) ([updated post on K-A](https://krita-artists.org/t/text-tool-thread/57973/132?u=sooz))

### Other Development Highlights

Google Summer of Code student Ken Lo's project for a pixel-art line stabilizer was finished successfully.

There’s a new default Python plugin, the Workflow Buttons docker, with customizable buttons that can select a tool, brush, color, or run a script.

![Workflow Buttons screenshot](images/posts/2024/mu18_workflow_buttons-freyalupen.png)

The team plans to make a 5.2.5 release in late September, containing various bug-fixes from the past few months. These fixes include issues with no layer being selected, bugs when triggering touch gestures and stylus actions at the same time, broken support for Deflate-using TIFF files on Windows, crashes importing audio on macOS, and many more.

After the release, the developers will lead a community bug hunt effort to reduce the number of open bug reports and fix bugs. Tune in next month for details.

### Part 2 of Krita’s 25-year history video released

[Krita 25th ANNIVERSARY! 🎉 Journey to the past. Part 2](https://www.youtube.com/watch?v=3Bi_1rX2AB4)

## Community Report

### July 2024 Monthly Art Challenge

The theme for July was [Still Life from Another World](https://krita-artists.org/t/monthly-art-challenge-july-2024-still-life-from-another-world/95264) designed by Brinck. We had so many wonderful submissions, it was difficult to choose only two when it was time to vote.

The winner of the July Art Challenge is [Alchemy Still Life](https://krita-artists.org/t/alchemy-still-life-july-2024-challenge-winner/97905) by @Elixiah.

![Alchemy Still Life by Elixiah](images/posts/2024/mu18_alchemy_still_life-elixiah.jpeg)

### August 2024 Monthly Art Challenge

Elixiah passed the honour of designing the next challenge to our first runner up, DavidMahl. For the August Art Challenge, DavidMahl has chosen “I am 5-year-old Kiki and I’m scared of …”

https://krita-artists.org/t/monthly-art-challenge-august-2024-i-am-5-year-old-kiki-and-im-scared-of/98020.

And the winner is… [5-year-old Kiki is scared of…the upgrade!](https://krita-artists.org/t/5-year-old-kiki-is-scared-of-the-upgrade-my-entry-to-august-24-challenge/100600/) by Lynx3d

![5-year-old Kiki is scared of…the upgrade! by Lynx3d](images/posts/2024/kiki_upgrade.jpeg)

### The September Art Challenge is Open Now

For the September Art Challenge, Lynx3d has chosen [Traditional Refreshments and Snacks](https://krita-artists.org/t/monthly-art-challenge-september-2024-topic-traditional-refreshments-and-snacks/100673). And as an additional, optional challenge, something with a seasonal or local relation. See the full brief for more details.

## Featured Artwork

Eleven images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-june-july-2024-nomination-submissions-thread/93833) which was open for nominations from June 14th to July 11th. When voting closed on the 14th, these five had the most votes and were added to the Krita-Artists featured artwork banner. These images will be entered into the Best of Krita Artists 2024 competition next January.

[The Golden Chamber](https://krita-artists.org/t/the-golden-chamber/94145) by @Yaroslavus_Artem

![The Golden Chamber by @Yaroslavus_Artem](images/posts/2024/mu18_the_golden_chamber-yaroslavus_artem.jpeg)

[Barn Owl on Faucet](https://krita-artists.org/t/barn-owl-on-faucet-final/94723) by @kacart

![Barn Owl on Faucet by @kacart](images/posts/2024/mu18_barn_owl_on_faucet-kacart.jpeg)

[Daal](https://krita-artists.org/t/daal-a-legume-colored-painting/93955) by @Neobscura

![Daal by @Neobscura](images/posts/2024/mu18_daal-neobscura.jpeg)

[Pixel Art Waterfall](https://krita-artists.org/t/pixel-art-waterfall-gif/88994) by @Katamaheen

![Pixel Art Waterfall by @Katamaheen](images/posts/2024/mu18_pixel_art_waterfall-katamaheen.gif)

[Indiana Jones](https://krita-artists.org/t/indiana-jones-trying-out-krita-for-the-first-time-as-a-photoshop-user/94223) by @AliceArt

![Indiana Jones by @AliceArt](images/posts/2024/mu18_indiana_jones-aliceart.jpeg)

### Best of Krita-Artists – July/August 2024

Six images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-july-august-2024-nomination-submissions-thread/96669/) which was open from July 15th to August 11th. When voting closed on August 14th, these five had the most votes and were added to the Krita-Artists featured artwork banner.

[The last supper](https://krita-artists.org/t/the-last-supper/97322) by @ahmet_tabak

![The last supper by @ahmet_tabak](images/posts/2024/mu18_the_last_supper-ahmet_tabak.jpeg)

[Piel de mango - Mango skin + process](https://krita-artists.org/t/piel-de-mango-mango-skin-process/96508) by @Caliche_Miguel

![Piel de mango - Mango skin + process by @Caliche_Miguel](images/posts/2024/mu18_piel_de_mango-caliche_miguel.jpeg)

[Somewhere in Alaska ~ Finished](https://krita-artists.org/t/somewhere-in-alaska-finished/97908) by @Elixiah

![Somewhere in Alaska ~ Finished by @Elixiah](images/posts/2024/mu18_somewhere_in_alaska-elixiah.jpeg)

[Melody.. using Impression brushes](https://krita-artists.org/t/melody-using-impression-brushes/96889) by @RoyKannthali

![Melody.. using Impression brushes by @RoyKannthali](images/posts/2024/mu18_melody-roykannthali.jpeg)

[Fish Tank Sea Monster](https://krita-artists.org/t/fish-tank-sea-monster/89593) by @Katamaheen

![Fish Tank Sea Monster by @Katamaheen](images/posts/2024/mu18_fish_tank_sea_monster-katamaheen.gif)

### Best of Krita-Artists – August/September 2024

The poll is open from September 11th to September 14th. [Cast your vote](https://krita-artists.org/t/best-of-krita-artists-voting-open-august-september-2024-nomination-submissions-thread/99354/19) for the best of Krita-Artists!

## Noteworthy Plugin

### New Alignment Tool

[Arrange 2: Universal alignment tools for all types of layers](https://krita-artists.org/t/arrange-2-universal-alignment-tools-for-all-types-of-layers/98672) by @Celes works on vector layers and raster (paint) layers as well as groups. Elements may be aligned to the active layer, the canvas or all selected layers.

To quote the author:
> I’m also still pretty new to Krita and might have overlooked some layer usage situations. Don’t hesitate to let me know!

{{< video-player src="videos/posts/2024/celes-krita__PLUGIN_DEMO__Arrange2_1.0.0.mp4" type="video/mp4" >}}

## Tutorial of the Month

Parallax Scrolling Animation

https://youtu.be/gsmcNqDpGJE?si=m8ITgkc-xBen-ypy


## Ways to Help Krita

Krita is a Free and Open Source application, mostly developed by an international team of enthusiastic volunteers. Donations from Krita users to support maintenance and development is appreciated.

Visit [Krita’s funding page](https://krita.org/en/donations/) to see how donations are used and explore a one-time or monthly contribution.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Code Changes

This section has been compiled by freyalupen.

(July 21 - Sept 6, 2024)

---

Stable branch (5.2.3+):
Bugfixes:

* [Layer Stack] Fix no layer being activated on opening Krita directly into a document, or opening a new view on a document, also when opening multiple documents and switching between them. And prevent a crash when pasting into a document with no layer active. ([BUG:490375](https://bugs.kde.org/490375), [BUG:490843](https://bugs.kde.org/490843), [BUG:490636](https://bugs.kde.org/490636)) ([commit 1](https://invent.kde.org/graphics/krita/-/commit/238e0e55b5), [commit 2](https://invent.kde.org/graphics/krita/-/commit/3b58918168), [commit 3](https://invent.kde.org/graphics/krita/-/commit/0e0bcc3c61), Dmitry Kazakov)
* [Layer Stack] Make sure that older files with simple transform masks load fine and fix endless updates with clone layer + transform mask. ([BUG:492320](https://bugs.kde.org/492320), [BUG:443766](https://bugs.kde.org/443766)) ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2228))
* [Animation Audio] Fix a freeze when playing audio while scrubbing the timeline. ([BUG:489146](https://bugs.kde.org/489146)) ([commit 1](https://invent.kde.org/dkazakov/krita-deps-management/-/commit/c0c8a366d6), [commit 2](https://invent.kde.org/graphics/krita/-/commit/348aeb8679), Dmitry Kazakov)
* [Input] Fix an input bug when triggering touch gestures and stylus at the same time. ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2196))
* [Brush Engines] Fix a crash in the Filter Brush when changing the filter type. ([BUG:478419](https://bugs.kde.org/478419)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/1368f13860))
* [Animation] Fix black canvas appearing in animation when "Limit cached frame size" is active and canvas filtering mode is Bilinear or Nearest Neighbor. ([BUG:486417](https://bugs.kde.org/486417)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/66e3b37f23))
* [File Formats: WebP] Fix issue in WebP colorspace export when dithering is enabled. ([BUG:491231](https://bugs.kde.org/491231)) ([merge request, Rasyuqa A H](https://invent.kde.org/graphics/krita/-/merge_requests/2212))
* [File Formats: PSD] Don't let Show Guides prevent saving as PSD in a non-default colorspace. ([BUG:492236](https://bugs.kde.org/492236)) ([commit, Halla Rempt](https://invent.kde.org/graphics/krita/-/commit/28a8d2d4d5))
* [Windows: File Formats: TIFF] Fix opening and saving TIFF files using Deflate compression on Windows, a regression in 5.2.3. ([BUG:489596](https://bugs.kde.org/489596)) ([commit, Dmitry Kazakov](https://invent.kde.org/dkazakov/krita-deps-management/-/commit/c1049ffb1b))
* [macOS: Animation Audio] Fix crash on importing audio on macOS, a regression in 5.2.3. ([BUG:490181](https://bugs.kde.org/490181)) ([commit, Ivan Yossi](https://invent.kde.org/graphics/krita/-/commit/965cd20df3))
* [Non-Windows: Display Settings] Enable HDR settings only on Windows, as Krita does not support HDR on other platforms and enabling it there will cause a crash. ([BUG:490301](https://bugs.kde.org/490301)) ([commit, Halla Rempt](https://invent.kde.org/graphics/krita/-/commit/3cfd68df06))
* [Compositions Docker] Fix crash when attempting to delete a composition after the last one has already been deleted. ([merge request, Ralek Kolemios](https://invent.kde.org/graphics/krita/-/merge_requests/2205))
* [Popup Palette] Fix the On-Canvas Brush Editor's decimal sliders to not round external changes. ([BUG:447800](https://bugs.kde.org/447800), [BUG:457744](https://bugs.kde.org/457744)) ([commit, Freya Lupen](https://invent.kde.org/graphics/krita/-/commit/f7fa3831aa))
* [File Formats: TIFF] Ignore resolution in TIFF if set to 0. Also don't allow setting an image's resolution to 0. ([BUG:473090](https://bugs.kde.org/473090)) ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2223))
* [Grids and Guides Docker] Fix scaling down an image to less than 50% size with 1px grid spacing enabled to not crash or have the grid disappear. ([BUG:490898](https://bugs.kde.org/490898)) ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2208))
* [Preferences Dialog] Make sure restoring default Canvas Input profile shows Krita Default. ([BUG:488478](https://bugs.kde.org/488478)) ([commit, Halla Rempt](https://invent.kde.org/graphics/krita/-/commit/546851b8dd))
* [Welcome Page] Fix loading translated News and add missing languages. ([BUG:489477](https://bugs.kde.org/489477)) ([CCBUG:489477](https://bugs.kde.org/489477)) ([commit 1](https://invent.kde.org/graphics/krita/-/commit/ae318ad7ae), [commit 2](https://invent.kde.org/graphics/krita/-/commit/a927059319), Halla Rempt)

Unstable branch (5.3.0-prealpha):
Features:

* [Text Tool] Add Text Properties Docker for styling text. ([merge request, Wolthera van Hövell](https://invent.kde.org/graphics/krita/-/merge_requests/2092))
* [Python Plugins] Add Workflow Buttons docker Python plugin. This docker consists of customizable buttons that can select a tool, brush, color, or even run a script. ([merge request, Timothée Giet](https://invent.kde.org/graphics/krita/-/merge_requests/2210))
* [Brush Engines] Add the ability to set brush sensor curve points as corners, by holding Ctrl while clicking a point, or pressing "s" while a point is selected. ([merge request, Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests/2191))
* [Freehand Brush Tool] Add Pixel stabilizer setting for drawing 1px-width pixel art lines. ([merge request, Ken Lo](https://invent.kde.org/graphics/krita/-/merge_requests/2158))
* [Touch Input] Implement an option to disable touch pressure reading, in case it is being reported incorrectly: Settings->General->Tools->Enable Touch Pressure Sensitivity. ([BUG:474523](https://bugs.kde.org/474523)) ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2197))
* [Color Selectors] Save global color history, and add option to save color history per document in Settings->Color Selector Settings->Color History. ([WISHBUG:357493](https://bugs.kde.org/357493)) ([merge request, Bourumir Wyngs](https://invent.kde.org/graphics/krita/-/merge_requests/1424))
* [Reference Images Tool] Add tool options to create a reference image from the current layer or visible canvas. ([WISHBUG:399058](https://bugs.kde.org/399058), [WISHBUG:403111](https://bugs.kde.org/403111)) ([merge request, Tom Wu, Agata Cacko](https://invent.kde.org/graphics/krita/-/merge_requests/636))
* [Usability] Add ability to change unit of decimal spinboxes in their right-click context menu, and fix some rounding issues when changing units. ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2199))
* [Usability] Reduce delay of Overview Docker, Histogram Docker and Layer Thumbnails updates. ([merge request, Dmitry Kazakov, Zhiqi Yao](https://invent.kde.org/graphics/krita/-/merge_requests/2180))
* [Usability] Change text to clarify "Merge selected layers" being different from "Merge with layer below". ([merge request, Ralek Kolemios](https://invent.kde.org/graphics/krita/-/merge_requests/2202))
* [Export Layers Plugin] Improve Export Layers dialog to select the current document, allow selecting exporting multiple documents at once, and show the full path only in the tooltip. ([merge request, stib bork](https://invent.kde.org/graphics/krita/-/merge_requests/2165))
* [Scripting] Add Python API for painting brushstrokes on a Node with the current brush: paintLine, paintRectangle, paintEllipse, paintPolygon, paintPath. ([merge request, Freya Lupen, Samuel Simplicio, Scott Petrovic](https://invent.kde.org/graphics/krita/-/merge_requests/2195))
* [Scripting] Expose stroke and fill styles to the painting API. ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2198))
* [Scripting] Add pressure setting to the Node.paintLine() function. ([merge request, Anna Pavlyuk](https://invent.kde.org/graphics/krita/-/merge_requests/2211))

Bugfixes:

* [Export] Fix Advanced Export of the image with filter masks or layer styles to scale those as expected.([BUG:476980](https://bugs.kde.org/476980)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/9c5277ffe0))
* [File Layers] Make sure File Layers are scaled when the image is scaled. ([BUG:467257](https://bugs.kde.org/467257), [BUG:470110](https://bugs.kde.org/470110)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/91aa790cab))
* [Transform Tool] Fix layers below a transformed layer appearing blurred when using Accurate Instant Preview mode. ([BUG:480973](https://bugs.kde.org/480973)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/72ba9e40a0))
* [Animation] Fix using Fill Tool on color-labeled layers of animation being very slow due to unnecessary checking of all frames. ([BUG:438607](https://bugs.kde.org/438607)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/5e791205dc))
* [Usability] Make sure the Layer Properties dialog can only be opened once, instead of allowing the same dialog to be opened multiple times. ([merge request, Maciej Jesionowski](https://invent.kde.org/graphics/krita/-/merge_requests/2189))
* [Python Plugins] Fix Ten Scripts to keep script position when there are empty slots, and add ability to clear a slot. ([merge request, Robert Moerland](https://invent.kde.org/graphics/krita/-/merge_requests/2215))
* [Python Plugins] Comics Manager: In the language chooser, list all regional variations and automatically select the best variation based on locale. ([merge request, Alvin Wong](https://invent.kde.org/graphics/krita/-/merge_requests/2226))
* [General] Fix wrong thumbnail in "Save New Brush Preset" after "Switch to Previous Preset" shortcut. ([BUG:475020](https://bugs.kde.org/475020)) ([merge request, Doreene Kang](https://invent.kde.org/graphics/krita/-/merge_requests/2230))

---

These changes are made available for testing in the following Nightly builds:

* Stable "Krita Plus" (5.2.3+): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)
* Unstable "Krita Next" (5.3.0-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)

{{< support-krita-callout >}}
