---
title: "25 Years of Krita!"
date: "2024-05-31"
author: Halla Rempt
---
* Halla Rempt

Twenty-five years. A quarter century. That's how long we've been working on Krita. Well, what would become Krita. It started out as KImageShop, but that name was nuked by a now long-dead German lawyer. Then it was renamed to Krayon, and that name was also nuked. Then it was renamed to Krita, and that name stuck.

I only became part of Krita in 2003, when Krita was still part of KDE's suite of productivity applications, KOffice, later renamed to Calligra... And I became [maintainer of Krita in 2004](https://marc.info/?l=kde-kimageshop&m=107877497900328&w=2), when Patrick Julien handed over the baton. That means that I've been around Krita for about twenty of those twenty-five years, so I'll hope you, dear reader, will forgive me for making this a really personal post; a very large part of my life has been tied up with Krita, and it's going to show.

But let’s first go back to before when I needed a digital painting application; the first seeds for Krita were laid in 1998, even earlier than the first bits of code. There was this excitement around Linux back then, and there were lots of projects that attempted to create great applications for Linux. One of those projects was GIMP, and another project was Qt. The first was a digital image manipulation application, the other was a toolkit to create user-friendly applications in C++. But GIMP didn't use Qt, it used its home-grown user interface toolkit (although it originally used Motif, which wasn't open source). A Qt fan, Matthias Ettrich, did an experimental port of GIMP to Qt, and gave a presentation about it at the [1998 Linux Kongress](http://www.linux-kongress.org/1998/abstracts.html). That wasn't received well, and resulted in the kind of spat that is typical of the open source community. People were young and tempers were hot.

Well, in cases like this, the only solution is to go at it yourself, and that's what happened. It took several false starts, but on the last day of May 1999, [Matthias Elter and Michael Koch started KImageShop](https://marc.info/?l=kde-devel&m=92815444812221&w=2): read the mail, because it's quite funny how we did and didn't follow the original vision (KOM was a Corba-like thing, and if you have never heard of Corba, that's probably because Corba was a terrible idea.).

![|Screenshot of the first mail](images/posts/2024/kimageshop_project_started.png)

Development started, and believe it or not, there's still some actual code dating back to then in Krita's codebase, though most of the remaining code is opening and closing brackets.

And then development stopped, because, well, doing a proper image manipulation application isn't easy or quick work. And then it started again, and stopped again, and started again. There were several maintainers before I was looking for a nice, performant codebase for a painting application in 2003. I didn't know C++; but I had written the first book on using Python and Qt together.

Krita had been rewritten to the point where it didn't even have a paint tool, so that was the first thing I wanted to have. That was not easy!

![|Screenshot of the first attempt at a brush engine](images/posts/2024/25_years_screenshot1.png)

But... Being open about it not being easy meant people got interested, and we started gaining contributors. And so, in 2004, we had a small team of enthusiastic people. A lot happened in that year; Camilla Boemann rewrote the core of Krita so we had autosizing layers, Adrian Page wrote an OpenGL based backend, Cyrille Berger added the first inklings of plugins and scripting. Our approach was still pretty technical, though, and we didn't manage to make a release.

It was only in 2005 that we released Krita as part of KOffice 1.4. Still very immature, but everyone agreed that it was promising, and we got nice reviews in some Linux magazines -- that was still a thing in 2005.

![|Screenshot of Krita 1.4](images/posts/2024/Krita_1.4.2.png)

Then came 2006. And Krita 1.5 was released with support for color managed CMYK. Krita 1.5 also had the short-lived real color mixing watercolor layer feature, but that was too complex to maintain. And in the same year, we released Krita 1.6: Linux Journal called it [State of the Art](https://www.linux.com/news/krita-16-state-art/). We thought it was a pretty mature release, but artists who gave us feedback still found it lacking a lot.

And then disaster struck. Qt3 reached end-of-life, and Qt4 was released. The porting effort was huge and took ages, also because we, foolishly, decided to rewrite a lot of the 1.x code to make it possible to share components between KOffice applications. The rewrite took all of 2007, 2008 and half of 2009.

In the meantime, when we were desperately trying to fix all the bugs the porting and rewrite were introducing, we held our first fundraiser: that was to get Wacom tablets for testing Krita with, complete with art pens. I am still using the Wacom Intuos 3 we got way back then!

In 2009, we then released Krita 2.0. It was not really usable, but it was important for us to have something out that we could get people to test. Krita 2.1 was also released in 2009. We also got our [first sponsored developer, Lukáš Tvrdý](https://dot.kde.org/2009/12/02/krita-team-seeking-sponsorship-take-krita-next-level), whose task specifically was to fix all bugs. Later on, he also improved the performance of Krita's brushes.

As Krita gained recognition, we got more and more feedback, and in 2010, we decided to have a big sprint in Deventer where we were going to determine what we wanted Krita to be for our users. A Photoshop clone? A GIMP clone? A Corel Painter Clone? Or something that was itself. Who were we making Krita for?

The answer is true to today: we are making Krita for digital artists who are making art, mostly from scratch. Painting with Krita should be fun for artists of all kinds, all over the world.

But it would be some time before we'd reach that goal. 2010 saw Krita 2.2 and Krita 2.3: we thought that Krita 2.3 was ready for artists, but it was only with Krita 2.4 and 2.5 in 2012 that Krita really became pretty good! In fact, we had a laser-precise focus: for some years our rallying call was “Make Krita usable for David Revoy!” – partly silly, but also partly serious. We spent time during dev sprints observing artists and allowing them to live comment on what they liked and didn’t like, without the observing developers being allowed to open their mouths, wether in rebuttal or to help the artist out.

In the meantime, I had created the Krita Foundation so we could do fund-raisers to sponsor full-time developers. The first developer we sponsored was Dmitry Kazakov, who is still the lead developer for Krita.

Back then, Krita was still part of KDE's office suite, but it was called Calligra now, because of an interminable conflict with just one KOffice developer, the KWord maintainer. All that energy spent on that conflict could have gone into development, it was a huge waste. From the Calligra days onwards, development went much smoother. Nokia was now involved with Calligra's development, and the resulting improvement in the central libraries all applications used also helped improve Krita, though, conversely, the complexity needed to support a very diverse set of applications is still burdening us today.

Years went by. 2013 was completely uneventful. We made our releases (2.6, 2.7), did our fund-raisers, added features (like animation support), created a version of Krita with a special user interface for touch/tablet users (sponsored by Intel: we still have a great relationship with Intel, our main development fund sponsor). It was great to see the art people were creating, great to get feedback from users and just plain fun to tackle development.

![|Screenshot of Krita Sketch](images/posts/2013/kritasketch_2.png)

In 2014 we ported Krita to Windows, also because of the touch/tablet version of Krita. And we released eleven versions of Krita 2.9, which was really a very fine release.

Also in 2014, we had our first Kickstarter campaign. Kickstarter was new and fresh back then, and it was really exciting. We got nearly 700 people to sponsor Krita! And we ported Krita to MacOS. For some time we would do a Kickstarter campaign every year, and they were fun both for us and for our developers, we'd set stretch goals and let people vote on what they wanted us to work on.

![|The first kickstarter page](images/posts/2014/kickstarter.png)

I still had a day job back then, so it was all work done in the evenings and weekends, and on the train during my commute.

We also started porting Krita, again, this time to Qt5. That wasn't as hard as the port from Qt3 to Qt4, but we lost support for the tablet version of Krita because Qt5 made it impossible to properly integrate our OpenGL based canvas in the touch version of Qt5's libraries. We spent months and quite a bit of money on that, but it was no-go.

Then I broke my shoulder and lost my day job, with Blue Systems, and suddenly the Krita Foundation needed to pay me, too. Fortunately, we found a sponsor for the port to Qt5, and that was my first sponsored project.

In 2016, we released Krita 3.0 -- it wasn't as good as Krita 2.9, but thankfully we still remembered the pain we had when doing a rewrite combined with a port, so we simply did the port first, and didn't combine it with a huge rewrite. This had animation!

![|The first kickstarter page](images/posts/2024/krita_animation2.gif)

We also released our first and last paper artbook. A huge amount of work for me, which already started in 2015 and in the end, a huge money sink, too.

![|The artbook](images/posts/2017/artbook-spread.jpg)

We worked on improved versions of Krita 3.0 all through 2016 and 2017. 2018 rolled by, and we released Krita 4.0, with the results of Kickstarter-sponsored work. Though not all of it, because in 2017, I was preoccupied with the Great Tax Disaster. The Dutch tax office wanted us to pay tens of thousands of euros in VAT for the work Dmitry had done; that's when we hired a proper accountant instead of a small business administration office in a local town.

When we went [public with the problems](https://krita.org/en/posts/2017/krita-foundation-in-trouble/), donations streamed in and PIA made a huge donation: they basically covered the bill.

To avoid having this happen again, I brought all commercial activities into a separate one-person company. That became even more important, because in 2017, we put Krita in the Windows Store. That was the second store, after we put Krita on the Steam Store in 2014. Since then, we have released Krita on Epic Store, the Google Play Store and now even on the Apple MacOS Store.

Time went on, and in 2018, we released Krita 4.1, in 2019 4.2, in 2020 Krita 4.3 and 4.4. Reasonably quiet years of active development, growing user base and popularity. More and more sponsored developers joined in, and Krita made a lot of progress.

Although the [Krita YouTube channel](https://www.youtube.com/c/KritaOrgPainting) already existed, in 2019, we asked Ramon Miranda to work on regular videos for our channel:

{{< youtube _9zZNHj5A_Y >}}

By now we've built up quite a list of impressive tutorials of all kinds, teaching everything from digital painting itself to creating brush presets!

And then development slowed down. In 2020, the effects of Covid19 became more and more clear. We couldn't have sprints anymore, so no hyper-productive in-person development sessions anymore. Team members got sick, for some, really sick. Long Covid has crashed my own productivity: there are many days when I can do nothing but lie down in a darkened room.

By 2021, even though we hadn't had to port Krita to a new version of Krita, we still decided to change vector layers from ODG to SVG, which made Krita files incompatible between versions 4 and 5. A major change in file format, in other words. We're still working on new versions of Krita 5: 5.1 in 2022, 5.2 in 2023.

The future promises a very nice Krita 5.3!

And also, groan, a Krita 6.0 because we have started porting Krita to Qt6. And that's no fun, because Qt6 is again a huge change in what Qt offers and allows.

And that was 25 years of working on something I started dabbling in because I wanted to draw a map for a fantasy novel on my laptop!

---
Join the [Development Fund](https://fund.krita.org/) with a monthly donation. Or make a one-time donation [here](https://krita.org/en/donations/).
![donate to krita](images/pages/2021-11-16_kiki-piggy-bank_krita5.png)


