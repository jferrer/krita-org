---
title: "New Video by Ramon Miranda: Seamless Templates"
date: "2024-11-10"
---
We've released a new video! ..Templates! you can create them with Krita to save time in
your digital paintings and be more focused on your artwork. They are seamless, they are useful, and they are really easy to generate. So let´s go with another supercool feature of Krita. 

Check out the video for download links for the templates themselves!

{{< youtube O6ii50NRdzA >}}
