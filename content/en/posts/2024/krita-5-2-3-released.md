---
title: "Krita 5.2.3 Released!"
date: "2024-06-26"
categories: 
  - "news"
  - "officialrelease"
---

Krita 5.2.3 is released after several weeks of testing of the [beta](/posts/2024/krita-5-2-3-beta1). This is a bugfix release, that primarily brings a complete overhaul of our build system, making it so that our CI system can now build for all 4 platforms (a Continuous Integration system basically builds a program after every change, runs some tests and based on that helps us track down mistakes we made when changing Krita's code).

Beyond the rework of the build system, this release also has numerous fixes, particularly with regards to animated transform masks, jpeg-xl support, shortcut handling on Windows and painting assistants.

In addition to the core team, special thanks goes out to to Freya Lupen, Grum 999, Mathias Wein, Nabil Maghfur Usman, Alvin Wong, Deif Lou, Maciej Jesionowski and Rasyuqa A. H. for various fixes, as well as the first time contributors in this release cycle (Each mentioned after their contribution).

## Changes since 5.2.3 beta:

- Various fixes to tool canvas input shortcut behaviour (Thanks, Aqaao)
- Improved icons for Android ([Bug 463043](https://bugs.kde.org/show_bug.cgi?id=463043), thanks Jesse 205!)
- Various fixes to how we use MLT for synchronising audio to animation. 
- Python SIP type stub generation, this will help autocompletion in external python editors that support using these stubs (Thanks Kate Corcoran)
- Crash fix with adding animation keyframe column on locked layer ([Bug 486893](https://bugs.kde.org/show_bug.cgi?id=486893))
- Fix update of "read-only" state of the document when loading and saving ([Bug 487544](https://bugs.kde.org/show_bug.cgi?id=487544))
- Ask to use PSD data in TIFF only if any was found ([Bug 488024](https://bugs.kde.org/show_bug.cgi?id=488024))
- Reworked default FFmpeg profiles ([Bug 455006](https://bugs.kde.org/show_bug.cgi?id=455006), [450790](https://bugs.kde.org/show_bug.cgi?id=450790), [429326](https://bugs.kde.org/show_bug.cgi?id=429326), [485515](https://bugs.kde.org/show_bug.cgi?id=485515), [485514](https://bugs.kde.org/show_bug.cgi?id=485514), thanks Ralek Kolemios!)
- Fix issue in KisMergeLabeledLayersCommand when masks where involved ([Bug 486419](https://bugs.kde.org/show_bug.cgi?id=486419))
- Update batch exporter Python plugin to fix trim option issue ([Bug 488343](https://bugs.kde.org/show_bug.cgi?id=488343), thanks Nathan Lovato!)
- Welcome Page: Fix "DEV BUILD" button going to a 404 (Thanks Joshua Goins!)
- Tablet Tester: Fix extreme lag with S Pen on Android (Thanks Joshua Goins!)
- Fix canvas fade-out when in 16-bit-uint mode on Angle ([Bug 488126](https://bugs.kde.org/show_bug.cgi?id=488126))
- WEBP & JPEG-XL: preemptive check for animation ([Bug 476761](https://bugs.kde.org/show_bug.cgi?id=476761))
- Fix copy-pasting selection of File Layer ([Bug 459849](https://bugs.kde.org/show_bug.cgi?id=459849))
- Fix color sampler in wrap around mode ([Bug 478190](https://bugs.kde.org/show_bug.cgi?id=478190))
- Replace old QML touch docker with QWidget-based touch docker to avoid problems on Android ([Bug 476690](https://bugs.kde.org/show_bug.cgi?id=476690))
- Add support for XSIMD13
- Redraw layers docker thumbnails if the canvas checkers color was changed
- Fix animation playback freezes when pausing past the end of audio ([Bug 487371](https://bugs.kde.org/show_bug.cgi?id=487371) and [478185](https://bugs.kde.org/show_bug.cgi?id=478185))

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-5.2.3-setup.exe](https://download.kde.org/stable/krita/5.2.3/krita-x64-5.2.3-setup.exe)
- Portable 64 bits Windows: [krita-5.2.3.zip](https://download.kde.org/stable/krita/5.2.3/krita-x64-5.2.3.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.3/krita-x64-5.2.3-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.3-x86\_64.appimage](https://download.kde.org/stable/krita/5.2.3/krita-5.2.3-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: We're not supporting MacOS 10.13 anymore, 10.14 is the minimum supported version.

- macOS disk image: [krita-5.2.3.dmg](https://download.kde.org/stable/krita/5.2.3/krita-5.2.3.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

We are no longer building the `32 bits Intel CPU APK`, as we suspect it was only installed by accident and none of our users actually used it. We are hoping for feedback on this matter.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.3/krita-x86_64-5.2.3-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.3/krita-arm64-v8a-5.2.3-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.3/krita-armeabi-v7a-5.2.3-release-signed.apk)

### Source code

- [krita-5.2.3.tar.gz](https://download.kde.org/stable/krita/5.2.3/krita-5.2.3.tar.gz)
- [krita-5.2.3.tar.xz](https://download.kde.org/stable/krita/5.2.3/krita-5.2.3.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.3/](https://download.kde.org/stable/krita/5.2.3) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/dmitry_kazakov.gpg). The signatures are [here](https://download.kde.org/stable/krita/5.2.3/) (filenames ending in .sig).
