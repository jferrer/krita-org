---
title: "Krita in 2022 and 2023"
date: "2023-04-03"
---

This is our, very late, yearly overview of the year past, and look-forward to the year that has just started. If that sort of thing bores you, feel free to skip!

![](images/posts/2023/electrichearts_20201224A_kiki_c1-1024x512.png)

Like in 2021 and 2020, no members of the Krita team actually died from the ongoing pandemic. This feels a bit miraculous, but there it is. The year was pretty awful, though, with the Krita maintainer getting long COVID (that's me, Halla...) and being too sick to do anything four days out of seven -- and various other upheavals we're not going to detail because that would not be safe, or would be too private to talk about, we really had a really tough year.

The years before 2022 were mostly marked by us trying, sometimes desperately, to keep on top of the number of bug reports flowing in. A lot of bug reports, it has to be said, are basically not actionable. That's not to blame the reporter who went through the lengths needed to register and then write up their problem. Which is really an admirable amount of effort. But too often, the problems are with the OS support for tablets, display drivers... And plain misunderstandings. And cries for user support. But there are now more open bug reports than a year ago.

![](images/posts/2023/bugs_2022.png)

So, for this year, we decided to take a different approach. We wanted to pro-actively determine what _we_ wanted to work on, and then discuss how to do that, and go for it, instead of just reacting to bug reports, wish bugs and feature requests.

So, early 2022, as soon as the lifting of lock-downs allowed, two Krita developers, Halla and Wolthera, sat together in Deventer, to prepare this idea. Of course, even before the invasion of Ukraine and all the problems _that_ caused, we couldn't get a full Krita Developers' Sprint together. So, this was limited.

We sat down, the two of us, and discussed what Krita's main commercial competition was. Well, that's Clip Studio, of course. Photoshop is not really making progress when it comes to painting support, and Corel Painter is not an application we would take as our example: we want to create the most efficient application to create new images with. Turning photo's into something painterly is not our goal.

The result of that was a [document where we tried to identify the most important things that were missing](https://docs.google.com/document/d/18ZL_DiMc3VdtcN_dCz_BTMn1fR3M72GuRw2E4ThRdiw), or lacking, in Krita. This is what we came up with:

- Text Object Features
- Single images vs Books or Projects
- Expanded Front Page
- Cloud Integration
- A Mobile UI
- Tracing over 3D Models
- Flexible Comic Paneling
- Vector Brushes
- Layer Management Improvements
- Project Asset Management
- New Fill Tools

Then we had [a virtual meeting with the whole team of sponsored developers](https://docs.google.com/document/d/1GPcclrQbxZN7iDeSP6Zo3CckEN8rrotwYozjmjktmX8), and allowed people to pick things _they_ were interested in:

- Dmitry was going to focus on splitting up the logic in the brush editors so we could create new brush editor interfaces without breaking the brush engines. This is now done.
- Text Object. Making a clear distinction between the text object and the text tool, Wolthera spent an enormous amount of work on improving the text object. (NOTE: this means the text tool itself hasn't been updated, so you need to provide the raw SVG at the moment.) The text object now supports SVG2 and CSS, with word-wrap and everything. This image shows the result of her work:

![](images/posts/2023/image.png)

This is is now also mostly done and merged.


- Agata has been working on improving the [user experience of Krita's assistants feature](https://phabricator.kde.org/T13380) and intends to work on flexible comic book paneling. The first is now in review ([1](https://invent.kde.org/graphics/krita/-/merge_requests/1563), 2), the second still needs to be started from the design up.

- Sharaf has been [working on porting the welcome page to QML](https://invent.kde.org/graphics/krita/-/merge_requests/1522) and to make it more flexible and useful. This is slow going, since he also needs to maintain Krita on Android and ChromeOS and the frequent API changes make that challenging. Another challenge is the KDE QML framework, Kirigami, which is in a permanent state of flux, and is not as stable as we'd hoped.
- Emmet (and before December 2022 Eoin) are working on overhauling the audio support in the animation system. [Technically this is ready](https://invent.kde.org/graphics/krita/-/merge_requests/1323), but there are problems with building the supporting libraries on all platforms.

And, as for bugs, we _did_ fix more than a thousand reports, of course.

We also made a bunch of releases:

- 5.0.6: 27-04-2022, [release announcement](https://krita.org/en/posts/2022/krita-5-0-6-released/)
- 5.0.8: 25-05-2022, this was a source-only release fixing a build regression for Linux distributions with Qt > 5.12.
- 5.1.0: 18-08-2022, [release announcement](https://krita.org/en/posts/2022/krita-5-1-0-released/), [release notes](en/release-notes/krita-5-1-release-notes/)
- 5.1.1: 13-09-2022, [release announcement](https://krita.org/en/posts/2022/krita-5-1-1-released/)
- 5.1.3: 7-11-2022, [release announcement](https://krita.org/en/posts/2022/krita-5-1-3-released/) (There was no 5.1.2)
- 5.1.4: 14-12-2022, [release announcement](https://krita.org/en/posts/2022/krita-5-1-4-released/)

Although we didn't manage to get Krita into the macOS (NOT the iPadOS) store -- the sand-boxing for store apps on macOS completely defeated us, and we didn't manage to find a way to make it work. Yet. On the Stores front, talking about that, we didn't see a huge decline in Steam or Windows Store sales -- though it was noticeable. Google Play and Epic only brought in pocket money.

Given the huge pressure _everyone_ is under, it's natural that the Krita Development Fund and the legacy dev funds as well as the occasional donations after downloading would decline. That happened, but on the plus side, we got [out first corporate sponsor](https://krita.org/en/posts/2022/intel-becomes-first-krita-development-fund-corporate-gold-patron/): Intel:


![](images/posts/2023/Intel_logo-classicblue-3000px.png)

![](images/posts/2023/krita_logo_extra.png)

But if we want to continue like this, well, we will need more money, there's no way around that. This year, 2023, will be a year of utter uncertainty.

When it comes to sponsored developers, we have almost the whole team still together: Sharaf, Dmitry, Halla, Wolthera, Tiar, Emmet, Ivan, Amy. Eoin left at the end of the year to start working on Godot, another really interesting free software project.

But we _were_ plagued by regressions not even our suite of unittests could stop before we released. That's kind of cyclical, though. Krita releases seem to go from stable to stable to regrettable to stable in response to something, something something we haven't determined. Which is why we make betas of course! Please test them!

All in all, 2022 was, like I said, _tough_. We made amazing progress given the constraints. But it was a year that tried temper, both in the developer community, as well as in the wider Krita community, not only the part of the community that actually talks to the developer team, but satellite communities like discord or reddit.

People often got pissed off at what they saw as lack of responsiveness from "the devs". While [krita-artists.org](http://krita-artists.org) is a great success, and a place where people are really helping each other out, it's also true that it's impossible for Krita's developers to read all the suggestions and feature requests and check out the often amazingly detailed mock-ups. We simply cannot keep up!

So, if you want to get involved in growing and improving Krita, you need to get in touch with Krita's developers directly. Either on IRC (or Matrix), by making a merge request on invent.kde.org or through the mailing list. And even then, keep in mind we're fighting a flood of bug reports all the while trying to implement features and improvements we've already decided have top priority!

What's to come in 2023?

Probably much delayed, we will release Krita 5.2.0. It will be delayed because after working through 2022, many team members are exhausted and need a longer break than usual. The problems that plagued us in 2022 are not gone, and some of us have had to move to new countries, either forced, or for study. And I've only had energy for project management, and have barely done any coding!

But Krita 5.2.0 will have all, or most, of the work listed above. You can track what's in Krita 5.2.0 using KDE's gitlab's milestone feature, while we're working on the release notes: [milestones](https://invent.kde.org/graphics/krita/-/milestones/5#tab-merge-requests). After 5.2.0, we'll be working on bugfix releases -- and of course the other things from our priority list that haven't been started yet!

If you want to see this happen, please support Krita, either with a [one-time donation](https://krita.org/en/donations/), a [subscription to the development fund](http://fund.krita.org) or by getting Krita in one of the supported app stores!
