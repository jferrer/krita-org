---
title: "Broken Rules Sponsors Krita!"
date: "2023-12-06"
---

![Broken Rules](images/sponsors/broken-rules-logo.png)

[Broken Rules](https://www.brokenrul.es/), the Vienna-based game development studio known for their award-winning games [Old Man's Journey](http://oldmansjourney.com/) and [Gibbon: Beyond the Trees](https://gibbongame.com/), became a Krita sponsor earlier this year.

Known for their thoughful and candid approach to game making it comes as no surprise that their Co-Founder and Creative Director Clemens Scott has a strong stance on software projects:

"In a world full of subscription services, forced updates, feature bloat and vendor lock-ins, it's refreshing to see that alternatives are cropping up. Projects such as Blender have proven that free, open-source software can reach industry-grade levels of quality and provide a viable alternative to the status quo. I've been using Krita on a regular basis for the past two years and I would love to see it become as ubiquituous as its corporate competitors."

[![Painting of a small narrow house perched precariously on a cliff. Next to it a very nautical-looking banner is floating above the sea, with an anchor, rope, a landscape with a lighthouse and the words "Old Man's Journey".](images/posts/2023/br_banner_omj.jpg)](images/posts/2023/br_banner_omj.jpg) 

Old Man's Journey was the Apple Design Award Winner 2017 and iPad Game of the Year 2017.


[![Very bright painting of a gibbon moving from a lush green forest on the left to a fiery red area on the right. Text above the gibbon: "Gibbon: Beyond the Tree".](images/posts/2023/br_banner_gibbon.jpg)](images/posts/2023/br_banner_gibbon.jpg) 

Gibbon: Beyond the tree won the Apple Design Award in 2022 in the category Social Impact.

[![An alien-looking landscape with alien-looking figures. One is playing a drum with two thin sticks, the other is leaning against a green hill.](images/posts/2023/br_banner_eloh.jpg)](images/posts/2023/br_banner_eloh.jpg) Eloh won the Apple Design Award in 2019.

Clemens used Krita mainly for personal projects, such as various illustrations throughout his [memex](https://nchrs.xyz/), but also for sketching out ideas on level designs for Gibbon: Beyond the Trees, which you can [find on his personal page](https://nchrs.xyz/gibbon.html) as well.

**We appreciate their support and contribution to keeping Krita going strong!**

### About the Studio

Broken Rules was founded in 2009 and has a track record of creative left-field works. Their games focus on originality, simplicity, refined game mechanics and rich interactivity.

Broken Rules founding members regard video games as important cultural achievements and seek to make every game a new and meaningful experience that lingers in the players' minds long after they powered down their computers.
