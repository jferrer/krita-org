---
title: "New Video by Ramon Miranda:"
date: "2023-05-03"
categories: 
  - "news"
  - "tutorials"
---

In this fifth video on creating brush presets in Krita, Ramon goes in depth with the masked brush feature!

 {{< youtube lpRSIYf4KFU >}}