---
title: "Krita 5.2 released!"
date: "2023-10-11"
categories: 
  - "news"
  - "officialrelease"
---

After a year of hard work, Krita 5.2 is finally here, bringing [a variety of new features](en/release-notes/krita-5-2-release-notes), ranging from fundamental changes in text and animation-audio handling to various smaller items like transforming all selected layers!

{{< youtube DAkXhTdARuI >}}

The following fixes were made in response to all your bug reports for the [first release candidate](https://krita.org/en/posts/2023/krita-5-2-release-candidate-is-out/):

- Set updateRect to widget rect on fractional hidpi screen ([Bug 441216](https://bugs.kde.org/show_bug.cgi?id=441216))
- Fix link to Scripting API in About splash (Thanks, Matt Alexander)
- Fix loading swatchbook CMYK palettes ([Bug 474583](https://bugs.kde.org/show_bug.cgi?id=474583))
- Remove a warning from KisTransactionData…possiblySwitchCurrentTime() ([Bug 474123](https://bugs.kde.org/show_bug.cgi?id=474123))
- Several QImage optimizations from Fushan Wen
- Fix crash when copy-pasting a transformation mask ([Bug 474673](https://bugs.kde.org/show_bug.cgi?id=474673))
- Make sure that saved encoder settings are supported by FFmpeg ([Bug 474559](https://bugs.kde.org/show_bug.cgi?id=474559))
- Fix external FFmpeg not working on Linux due to clash in LD\_LIBRARY\_PATH ([Bug 473603](https://bugs.kde.org/show_bug.cgi?id=473603))
- Make sure that splitter state is saved in Preset Chooser ([Bug 474669](https://bugs.kde.org/show_bug.cgi?id=474669))
- Update webp to the latest version
- Fix setting size of text brushtip
- Fix removal of duplicated frames in “Only Unique Frames” mode ([Bug 450449](https://bugs.kde.org/show_bug.cgi?id=450449))
- Added openh264 presets to the recorder plugin ([Bug 474803](https://bugs.kde.org/show_bug.cgi?id=474803))
- Fix layer selection menus piling up until closing the canvas
- Fix a crash after removing a vector layer with active selection of shapes ([Bug 474476](https://bugs.kde.org/show_bug.cgi?id=474476))
- Fix detection of FFmpeg in AppImages ([Bug 474927](https://bugs.kde.org/show_bug.cgi?id=474927))
- Fix misbehavior in the “grow until darkest pixel” selection filter ([Bug 475014](https://bugs.kde.org/show_bug.cgi?id=475014))
- Welcome Page: Added bare minimum close/hide button to dev fund banner.
- Update libvpx to 1.13.1 that fixes 0-day vulnerability
- Fix brush rotation when using Airbrush mode ([Bug 455627](https://bugs.kde.org/show_bug.cgi?id=455627))

In addition to the many new features listed in the [release notes](en/release-notes/krita-5-2-release-notes), there is also an updated welcome screen with larger thumbnails for recent images:

![](images/posts/2023/5.2.0-2-1024x686.png)


## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.0-setup.exe](https://download.kde.org/stable/krita/5.2.0/krita-x64-5.2.0-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.0.zip](https://download.kde.org/stable/krita/5.2.0/krita-x64-5.2.0.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.0/krita-x64-5.2.0-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.0-x86\_64.appimage](https://download.kde.org/stable/krita/5.2.0/krita-5.2.0-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.0.dmg](https://download.kde.org/stable/krita/5.2.0/krita-5.2.0.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.0/krita-x86_64-5.2.0-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.0/krita-x86-5.2.0-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.0/krita-arm64-v8a-5.2.0-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.0/krita-armeabi-v7a-5.2.0-release-signed.apk)

### Source code

- [krita-5.2.0.tar.gz](https://download.kde.org/stable/krita/5.2.0/krita-5.2.0.tar.gz)
- [krita-5.2.0.tar.xz](https://download.kde.org/stable/krita/5.2.0/krita-5.2.0.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.0/](https://download.kde.org/stable/krita/5.2.0) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. This particular release is signed with a non-standard key, you can retrieve is [here](https://files.kde.org/krita/4DA79EDA231C852B) or download from the public server:

gpg --recv-keys E9FB29E74ADEACC5E3035B8AB69EB4CF7468332F

The signatures are [here](https://download.kde.org/stable/krita/5.2.0/) (filenames ending in .sig).

{{< support-krita-callout >}}
