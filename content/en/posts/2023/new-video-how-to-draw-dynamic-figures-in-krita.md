---
title: "New Video: How to Draw Dynamic Figures in Krita"
date: "2023-03-09"
categories: 
  - "tutorials"
---

This week, we have released the latest of Ramon's Krita videos! This time there's even a [Blender](https://blender.org) file to help you pose figures!

 {{< youtube BlDNC1P0We0 >}}
