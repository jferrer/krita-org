---
title: "New Video: Memilio Impasta Brushes!"
date: "2025-02-05"
---

Ramon Miranda has published a new video on the Krita channel: Memilio Impasto Brushes! Who doesn't love a nice impasto effect!

{{< youtube YxMfMSzBaz4 >}}
