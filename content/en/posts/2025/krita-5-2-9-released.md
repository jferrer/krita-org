---
title: "Krita 5.2.9 Released!"
date: "2025-01-29"
categories: 
  - "news"
  - "officialrelease"
---

Today we're releasing Krita 5.2.9! This is a bug fix release, containing all bugfixes of our bug hunt efforts back in November. Major bug-fixes include fixes to clone-layers, fixes to opacity handling, in particular for file formats like Exr, a number of crash fixes and much more!

Special thanks to Doreene Kang, Ralek Kolemios, Pedro Reis, Guillaume Marrec, Aqaao, Grum999, Maciej Jesionowski, Freya Lupen, Dov Grobgeld, Rasyuqa A. H.

- Add shortcuts to bezier curve and freehand path
- Fix original offsets not being accounted for when copying from another document ([Bug 490998](https://bugs.kde.org/show_bug.cgi?id=490998))
- Fix updates when copy-pasting multiple nodes into a new document
- Fix infinite loop when trying to update pass-through nodes ([Bug 493774](https://bugs.kde.org/show_bug.cgi?id=493774), [Bug 493830](https://bugs.kde.org/show_bug.cgi?id=493830), [Bug 493837](https://bugs.kde.org/show_bug.cgi?id=493837))
- Fix Photobash Crash
- Bug fix: Added A1 and A2 sizes when creating a document
- Fix a crash when trying to merge down a layer created after a reference image addition
- Possibly fix saving EXR files with extremely low alpha values
- Fix infinite loop when a clone layer is connected to a group with clones
- Dynamic brush tool shouldn't load the saved smoothing configuration ([Bug 493249](https://bugs.kde.org/show_bug.cgi?id=493249))
- Fix bogus offset when saving EXR with moved layers
- Try to keep color space profile when saving EXR of incompatible type
- Fix crash when re-importing the same resource, but changed ([Bug 484796](https://bugs.kde.org/show_bug.cgi?id=484796))
- Fix range of Saturation and Value brush options ([Bug 487469](https://bugs.kde.org/show_bug.cgi?id=487469))
- Check pointer before dereferencing. ([Bug 479405](https://bugs.kde.org/show_bug.cgi?id=479405))
- Fix loading .kpp files with embedded top-level resources ([Bug 487866](https://bugs.kde.org/show_bug.cgi?id=487866), [Bug 456586](https://bugs.kde.org/show_bug.cgi?id=456586), [Bug 456197](https://bugs.kde.org/show_bug.cgi?id=456197))
- Fix a crash when trying to clear scratchpad while it is busy ([Bug 488800](https://bugs.kde.org/show_bug.cgi?id=488800))
- Fix the current preset thumbnail to be present in the preset save dialog ([Bug 488673](https://bugs.kde.org/show_bug.cgi?id=488673), [Bug 446792](https://bugs.kde.org/show_bug.cgi?id=446792))
- JPEG XL: Fix potential lockup when loading multipage images
- Set the correct shortcut for zoom in in the action file ([Bug 484365](https://bugs.kde.org/show_bug.cgi?id=484365))
- Fixed: some tools is interrupted by recorder. ([Bug 488472](https://bugs.kde.org/show_bug.cgi?id=488472), [Bug 477715](https://bugs.kde.org/show_bug.cgi?id=477715), [Bug 484783](https://bugs.kde.org/show_bug.cgi?id=484783))
- Make sure that the text tool is not interrupted by the recorder ([Bug 495768](https://bugs.kde.org/show_bug.cgi?id=495768))
- Recover "Clean Up" button in the Krita's Recordings Manager ([Bug 455207](https://bugs.kde.org/show_bug.cgi?id=455207))
- Fix a possible saving lockout due to incorrect ownership of the saving mutex ([Bug 496018](https://bugs.kde.org/show_bug.cgi?id=496018))
- Fixes to the unit spinboxes, a new context menu has been added to change the unit.
- Make vector and raster selections to behave in the same way when creating tiny selections ([Bug 445935](https://bugs.kde.org/show_bug.cgi?id=445935))
- Fix unclosed paths when intersecting two rectangular selections ([Bug 408369](https://bugs.kde.org/show_bug.cgi?id=408369))
- Fix crash when closing Krita while Calligraphy Tool is active ([Bug 496257](https://bugs.kde.org/show_bug.cgi?id=496257))
- Fix following existing shape in the Calligraphy Tool ([Bug 433288](https://bugs.kde.org/show_bug.cgi?id=433288))
- Fix "Copy into new Layer" action when working with vector layers ([Bug 418317](https://bugs.kde.org/show_bug.cgi?id=418317))
- Make sure that eraser button is properly initialized on Krita start ([Bug 408440](https://bugs.kde.org/show_bug.cgi?id=408440))
- Fix focus issues in Canvas Size dialog ([Bug 474809](https://bugs.kde.org/show_bug.cgi?id=474809))
- Disable snapping to image center by default ([Bug 466718](https://bugs.kde.org/show_bug.cgi?id=466718))
- Change case of AppImage packages: .appimage -> .AppImage ([Bug 447445](https://bugs.kde.org/show_bug.cgi?id=447445))
- Make sure that point-based snap points have higher priority than line-based ones ([Bug 492434](https://bugs.kde.org/show_bug.cgi?id=492434))
- Implement canvas decorations for Snap-to-guides feature
-Don't allow lowering/raising a mask into a locked layer
- Fix display profile conversion flags to be updated on settigns change ([Bug 496388](https://bugs.kde.org/show_bug.cgi?id=496388))
- Switch color history in the popup palette to use last-used sorting ([Bug 441900](https://bugs.kde.org/show_bug.cgi?id=441900))
- Add Unify Layers Color Space action
- Fix incorrect action text for "Paste Shape Style" ([Bug 497035](https://bugs.kde.org/show_bug.cgi?id=497035))
- Fix backward compatibility of Per-Channel filter ([Bug 497336](https://bugs.kde.org/show_bug.cgi?id=497336))
- Fix rendering of the warning icon in the composite op selector
- Simplify path point and control point move strategies.
- Fix an assert when modifying Mesh Gradient on a shape ([Bug 496519](https://bugs.kde.org/show_bug.cgi?id=496519))
- Fix aspect ratio of Resource Manager tooltips
- Improve rendering of pattern thumbnails
- Fix artifacts when painting under a gaussian blus layer in WA-mode ([Bug 434938](https://bugs.kde.org/show_bug.cgi?id=434938))
- Fix an assert when undoing merging of locked layers ([Bug 497389](https://bugs.kde.org/show_bug.cgi?id=497389))
- Remove ignoring of the mouse events in KoPathTool ([Bug 411855](https://bugs.kde.org/show_bug.cgi?id=411855))
- Use traceback instead of cgitb ([Bug 497859](https://bugs.kde.org/show_bug.cgi?id=497859))
- Fix ambiguous "break path" shortcut in Shape Edit Tool ([Bug 429503](https://bugs.kde.org/show_bug.cgi?id=429503))
- Add position indepndent property to libraqm
- Fix python Py_SetPath() deprecation by always using qputenv()
- JPEG XL export: Fix unable to set EPF value to -1 (encoder chooses)
- G'Mic has been updated to 3.5.0 stable.

## Download

### Windows

If you're using the *portable zip files*, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note: We are no longer making 32-bit Windows builds.

- 64 bits Windows Installer: [krita-5.2.9-setup.exe](https://download.kde.org/stable/krita/5.2.9/krita-x64-5.2.9-setup.exe)
- Portable 64 bits Windows: [krita-5.2.9.zip](https://download.kde.org/stable/krita/5.2.9/krita-x64-5.2.9.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.9/krita-x64-5.2.9-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.9-x86\_64.AppImage](https://download.kde.org/stable/krita/5.2.9/krita-5.2.9-x86_64.AppImage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: right-click on the link to download.)

### MacOS

Note: We're not supporting MacOS 10.13 anymore, 10.14 is the minimum supported version.

- MacOS disk image: [krita-5.2.9.dmg](https://download.kde.org/stable/krita/5.2.9/krita-5.2.9-release.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.9/krita-x86_64-5.2.9-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.9/krita-arm64-v8a-5.2.9-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.9/krita-armeabi-v7a-5.2.9-release-signed.apk)

### Source code

- [krita-5.2.9.tar.gz](https://download.kde.org/stable/krita/5.2.9/krita-5.2.9.tar.gz)
- [krita-5.2.9.tar.xz](https://download.kde.org/stable/krita/5.2.9/krita-5.2.9.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.9/](https://download.kde.org/stable/krita/5.2.9) and click on "Details" to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/dmitry_kazakov.gpg). The signatures are [here](https://download.kde.org/stable/krita/5.2.9/) (filenames ending in .sig).
