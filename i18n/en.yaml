# Global header and footer text
recent_posts:
  other: Recent Posts
donate:
  other: Donate
software:
  other: Software
education:
  other: Education
foundation:
  other: Foundation
language:
  other: Language
report-a-bug:
  other: Report a bug
roadmap:
  other: Roadmap
release-history:
  other: Release History
documentation:
  other: Documentation
source-code:
  other: Source Code
faq:
  other: FAQ
privacy-statement:
  other: Privacy Statement
tutorials:
  other: Tutorials
about:
  other: About
donation:
  other: Donations
get-involved:
  other: Get Involved
what-is-kde:
  other: What is KDE
website-license:
  other: Website license
contact:
  other: Contact
site-map:
  other: Sitemap
previous-post:
  other: Previous Post
next-post:
  other: Next Post
minutes:
  other: '{{ .ReadingTime }} minutes'
  one: One minute
reading-time:
  other: 'Reading time:'
# home and artist-interviews pages
interview-question:
  other: 'Could you tell us something about yourself? '

# Homepage
download:
  other: Download
powered-by:
  other: Powered by
art-by:
  other: 'Art by '
news: 
  other: News
no-news: 
  other: No News
see-all:
  other: See All
home-tools-to-grow:
  other: Tools You Need To Grow as an Artist
home-tools-feature-text: 
  other: All the features you need
home-tools-feature-description:
  other: 'Great for new and experienced artists. Here are some of the features that Krita offers:'
home-tools-feature-benefit-1:
  other: Multiple brush types for different art styles
home-tools-feature-benefit-2:
  other: Layers, drawing assistants, stabilizers
home-tools-feature-benefit-3:
  other: Animation tools to transform your artwork
resources-and-materials:
  other: Resources and Materials
resources-and-materials-description:
  other: "Expand Krita’s capabilities with online tools and assets:"
resources-and-materials-benefit-1:
  other: Brushes, patterns, and vector libraries
resources-and-materials-benefit-2:
  other: Texture packs and page templates
resources-and-materials-benefit-3:
  other: Plugins that add new functionality
see-resources:
  other: See Resources
home-tools-feature-button:
  other: See features
home-education-text:
  other: FREE education and resources
home-education-button:
  other: See education
home-community-text:
  other: A supportive community
home-community-description:
  other: An online community for Krita artists to share artwork and tips with each other.
home-community-benefit-1:
  other: Share your artwork and get feedback to improve
home-community-benefit-2:
  other: Ask questions about using Krita or seeing if there is a bug
home-community-benefit-3:
  other: Give feedback to developers when new features and plugins are being created
home-community-button:
  other: Visit artist community
artist-interviews:
  other: Artist Interviews
free-opensource:
  other: Free and Open Source
free-opensource-text:
  other: Krita is a public project licensed as GNU GPL, owned by its contributors. That's why Krita is Free and Open Source software, forever.
free-opensource-button:
  other: See details about license usage
give-back:
  other: Give back
give-back-description:
  other: Krita is made by people from all around the world – many of which are volunteers. If you find Krita valuable and you want to see it improve, consider becoming part of the development fund.
give-back-button:
  other: Contribute to the development fund
read-more:
  other: Read more
krita-sprint-caption:
  other: Krita Sprint 2019 - Deventer, Netherlands


# feature page
no-trials:
  other: No trials.
no-subscriptions:
  other: No subscriptions.
no-limits:
  other: No limit to your creativity.
clean-and-flexible:
  other: Clean and Flexible Interface
an-intuitive-interface:
  other: An intuitive user interface that stays out of your way. The dockers and panels can be moved and customized for your specific workflow. Once you have your setup, you can save it as your own workspace. You can also create your own shortcuts for commonly used tools.
customize-layout:
  other: Customizable Layout
over-30-docker:
  other: Over 30 dockers for additional functionality
dark-light-themes:
  other: Dark and light color themes
learn-the-interface:
  other: Learn the interface
all-the-tools-you-need:
  other: All the tools you need
over-100-brushes:
  other: Over 100 professionally made brushes that come preloaded. These brushes give a good range of effects so you can see the variety of brushes that Krita has to offer.
beautiful-brushes:
  other: Beautiful Brushes
learn-more:
  other: Learn more
brush-stabilizer:
  other: Brush Stabilizers
brush-stabilizer-description:
  other: Have a shaky hand? Add a stabilizer to your brush to smoothen it out. Krita includes 3 different ways to smooth and stabilize your brush strokes. There is even a dedicated Dynamic Brush tool where you can add drag and mass.
vector-and-text:
  other: Vector & Text
vector-and-text-description:
  other: Built-in vector tools help you create comic panels. Select a word bubble template from the vector library and drag it on your canvas. Change the anchor points to create your own shapes and libraries. Add text to your artwork as well with the text tool. Krita uses SVG to manage its vector format.
brush-engines:
  other: Brush Engines
brush-engines-description:
  other: Customize your brushes with over 9 unique brush engines. Each engine has a large amount of settings to customize your brush. Each brush engine is made to satisfy a specific need such as the Color Smudge engine, Shape engine, Particle engine, and even a filter engine. Once you are done creating your brushes, you can save them and organize them with Krita's unique tagging system.
wrap-around-mode:
  other: Wrap-around mode
wrap-around-mode-description:
  other: It is easy to create seamless textures and patterns now. The image will make references of itself along the x and y axis. Continue painting and watch all of the references update instantly. No more clunky offsetting to see how your image repeats itself.
resource-manager:
  other: Resource Manager
resource-manager-description:
  other: Import brush and texture packs from other artists to expand your tool set. If you create some brushes that you love, share them with the world by creating your own bundles. Check out the brush packs that are available in the Resources area.
resource-area-button:
  other: Visit Resources Area
animation-title:
  other: Simple and Powerful 2D Animation
animation-title-description:
  other: Turn Krita into an animation studio by switching to the animation workspace. Bring your drawings to life by layering your animations, importing audio, and fine tuning your frames. When you are finished, share with your friends by exporting your creation to a video. Or just export the images to continue working in another application.
features:
  other: Features
animation-feature-1:
  other: Multiple layers and audio support
animation-feature-2:
  other: Supports 1,000s of frames on timeline
animation-feature-3:
  other: Playback controls with pausing, playing, and timeline scrubbing
animation-feature-4:
  other: Onion skinning support to help with in-betweens
animation-feature-5:
  other: Tweening with opacity and position changes
animation-feature-6:
  other: Change start time, end time, and FPS
animation-feature-7:
  other: Export results to video or still images
animation-feature-8:
  other: Drag and drop frames to organize timings
animation-feature-9:
  other: Shortcuts for duplicating and pulling frames
animation-feature-10:
  other: Performance tweaking with drop-frame option
productivity-features:
  other: Productivity features
drawing-assistants: 
  other: Drawing Assistants
drawing-assistants-description: 
  other: Use a drawing aid to assist you with vanishing points and straight lines. The Assistant Tool comes with unique assistants to help you make that perfect shape. These tools range from drawing ellipses to creating curvilinear perspective with the Fisheye Point tool. 
layer-management:
  other: Layer Management
layer-management-description:
  other: In addition to painting, Krita comes with vector, filter, group, and file layers. Combine, order, and flatten layers to help your artwork stay organized.
select-and-transform:
  other: Select & Transform
select-and-transform-description:
  other: Highlight a portion of your drawing to work on. There are additional features that allow you to add and remove from the selection. You can further modify your selection by feathering and inverting it. Paint a selection with the Global Selection Mask.
full-color-management:
  other: Full Color Management
full-color-management-description:
  other: Krita supports full color management through LCMS for ICC and OpenColor IO for EXR. This allows you to incorporate Krita into your existing color management pipeline. Krita comes with a wide variety of ICC working space profiles for every need.
gpu-enhanced:
  other: GPU Enhanced
gpu-enhanced-description:
  other: With OpenGL or Direct3D enabled, you will see increased canvas rotation and zooming speed. The canvas will also look better when zoomed out. The Windows version supports Direct3D 11 in place of OpenGL.
psd-support:
  other: PSD Support
psd-support-description:
  other: Open PSD files that even Photoshop cannot open. Load and save to PSD when you need to take your artwork across different programs.
hdr-painting:
  other: HDR Painting
hdr-painting-description: 
  other: Krita is the only painting application that lets you open, save, edit and author HDR and scene-referred images. With OCIO and OpenEXR support, you can manipulate the view to examine HDR images.
python-scripting:
  other: Python Scripting
python-scripting-description:
  other: Powerful API for creating your own widgets and extending Krita. With using PyQt and Krita's own API, there are many possibilities. A number of plugins come pre-installed for your reference.
training-resources:
  other: Training Resources
training-resources-description:
  other: In addition to training and educational material found on the Internet, Krita produces its own training material to help you learn all of the tools fast.
all-of-this-for-free:
  other: All of this (and more) for FREE
all-of-this-for-free-description:
  other: Krita is, and will always be, free software. There is a lot more to learn than this overview page, but you should be getting a good idea of what Krita can do.


# Downloads page
download-krita:
  other: Download Krita
released-on:
  other: 'Released on: '
release-notes:
  other: Release Notes
windows-installer:
  other: Windows Installer
windows-portable:
  other: Windows Portable
mac-installer:
  other: macOS Installer
linux-appimage:
  other: Linux 64-bit AppImage
flatpak:
  other: Flatpak
snap:
  other: snap
hosted-on-flathub:
  other: Hosted on flathub.
maintained-by-community:
  other: Maintained by the community.
run:
  other: Run
iPadsiOSNotSupported:
  other: Apple iPads and iPhones are not supported
google-play-store:
  other: Google Play Store
see-android-apk-builds:
  other: See individual Android APK builds
system-requirements:
  other: System Requirements
system-requirements-colon:
  other: ' : '
operating-system:
  other: Operating System
operating-system-description:
  other: Windows 8.1 or higher / macOS 10.14 / Linux
ram:
  other: RAM
ram-description:
  other: 4 GB at minimum required. 16 GB recommended.
gpu-optional:
  other: GPU
gpu-optional-description: 
  other: OpenGL 3.0 or higher / Direct3D 11
graphics-tablet-brand:
  other: Graphics Tablet Brand
graphics-tablet-brand-description:
  other: Any tablet compatible with your operating system
store-version:
  other: Store version
store-version-description:
  other: Paid versions of Krita on other platforms. You will get automatic updates when new versions of Krita come out. After deduction of the Store fee, the money will support Krita development. For the Microsoft store version you will need Windows 10 or above.
windows-store:
  other: Microsoft Store
steam-store:
  other: Steam Store
epic-store:
  other: Epic Games Store
macos-store:
  other: Mac App Store
krita-next-builds:
  other: Krita Next Nightly Builds
krita-next-builds-description:
   other: New builds created daily that have bleeding-edge features. This is for testing purposes only. Use at your own risk. 
krita-next-builds-button:
  other: Visit Krita Next
krita-plus-builds:
  other: Krita Plus Nightly Builds
krita-plus-builds-description:
  other: Contains only bug fixes on top of the currently released version of Krita
krita-plus-builds-button:
  other: Visit Krita Plus
windows-shell-extension:
  other: Windows Shell Extension
windows-shell-extension-description: 
  other: The Shell extension is included with the Windows Installer. An optional add-on for Windows that allow KRA thumbnails to appear in your file browser.
windows: 
  other: Windows
mac-os:
  other: macOS
linux:
  other: Linux
source-code: 
  other: Source Code
source-code-description:
  other: Krita is a free and open source application. You are free to study, modify, and distribute Krita under GNU GPL v3 license.
download-older-versions:
  other: Download Older Versions
download-older-versions-description:
  other: If the newest version is giving you issues there are older versions available for download. New versions of Krita on Windows do not support 32-bit.
gpg-signatures:
  other: GPG Signatures
gpg-signatures-description:
  other: Used to verify the integrity of your downloads. If you don't know what GPG is you can ignore it.
download-signature:
  other: Download Signature
tarball:
  other: tarball
kde-repository:
  other: Git (KDE Invent)
old-version-library:
  other: Old Version Library
windows-32-bit-version:
  other: Last Windows 32-bit version
hosted-on-snapcraft:
  other: Hosted on snapcraft.




# donations overview
monthly-donations:
  other: Monthly Donations
monthly-donations-description:
  other: 'If you join the Krita Development Fund, you will directly help keep Krita getting better and better, and you will get the following:'
monthly-donations-benefit-1:
  other: A great application with powerful features, features designed together with the Krita community
monthly-donations-benefit-2:
  other: Stable software where we will always try to fix issues—last year over 1,200 issues were resolved!
monthly-donations-benefit-3:
  other: Visibility and recognition online, via community badges
visit-development-fund:
  other: Visit Development Fund

one-time-donation:
  other: One-time Donation
one-time-donation-description:
  other: One-time donations will take you to the Mollie payment portal. Donations are done in euros as that provides the most payment options.
accepted-payment-methods:
  other: 'Accepted payment methods:'


# One time donation page
transation-id:
  other: 'Transaction ID: '
paid-amount:
  other: 'Paid €%s'
transaction-issues-text:
  other: If there are issues with your donation, you can share this transaction ID with us to help us know exactly which donation was yours.
back-to-website:
  other: Back to website

# post-download page
have-fun-painting:
  other: Have Fun Painting
have-trouble-downloading:
  other: Having trouble downloading?
visit-downloads-area:
  other: Visit the Downloads area
whats-next:
  other: What's Next
connect-with-community:
  other: Connect with the Community
share-artwork-and-ask-questions:
  other: Share artwork and ask questions on the forum. 
krita-artists-forum:
  other: Krita Artists forum
watch-videos-to-learn:
  other: Watch videos to learn about new brushes and techniques.
krita-youtube-channel:
  other: Krita YouTube channel
free-learning-resources:
  other: Free Learning Resources

getting-started:
  other: Getting Started
user-interface:
  other: User Interface
basic-concepts: 
  other: Basic Concepts
drawing-assistants:
  other: Drawing Assistants
animations:
  other: Animations
tutorials:
  other: Tutorials
user-manual:
  other: User Manual
layers-and-masks:
  other: Layers and Masks


# support Krita callout
support-krita-callout-title:
  other: Like what we are doing? Help support us
support-krita-callout-description:
  other: Krita is a free and open source project. Please consider supporting the project with donations or by buying training videos or the artbook! With your support, we can keep the core team working on Krita full-time.
buy-something:
  other: Buy something


no-video-support-message:
  other: There should have been a video here but your browser does not seem
    to support it.


# 404 not found page
page-not-found-title:
  other: Nope. Not in here either... (404)
page-not-found-description:
  other: Either the page does not exist any more, or the web address was typed wrong.
back-to-homepage:
  other: Go back to the home page
